--debug

_log = {}
_log_sz = 50

function log(msg)
	-- clear old logs
	while #_log > _log_sz do
		deli(_log,1)
	end
	add(_log,msg)
end

dbg_str = {"debug mode"}
dbg_off = 0
dbg_en = false

function arr2str(arr)
	local delim = ""
	local str = "{"
	for _,v in ipairs(arr) do
		str = str..delim..tostr(v)
		delim = ","
	end
	return str.."}"
end

function dbg(x)
	if (dbg_en) then
		if (type(x) == "table") then
			add(dbg_str,arr2str(x))
		else
			add(dbg_str,x)
		end
	end
end

-- place this at the bottom of
-- the _update function
function update_dbg()
	-- player 1 controls toggle the
	-- debug mode
	if (btnp(❎,1)) then
		dbg_en = not dbg_en
		dbg_off = 0
	end
	
	if (dbg_en) then
		if (btn(⬇️,1)) then
			dbg_off = dbg_off+1
		elseif (btn(⬆️,1)) then
			dbg_off = 
				max(dbg_off-1,1)
		end
	end
end

-- place this at the bottom of
-- the _draw function
function print_dbg()
	if (dbg_en) then
		dbg"log:"
		dbg"===="
		for msg in all(_log) do
			dbg(msg)
		end
		dbg"===="

		dbg_off = 
			min(dbg_off,#dbg_str)
		-- there must be something
		-- more elegant than this...
		local l = max(dbg_off,1)
		      l = min(l,#dbg_str)
		local r = min(l+19,#dbg_str)
		
		local oldx,oldy = camera()

		cursor(0,0)
		cls(1)
		color(6)
		
		for i = l,r do
			print(dbg_str[i])
		end
		
		-- lua has good garbage 
		-- collection... right?
		dbg_str = {"debug mode"}
		camera(oldx, oldy)
	end
end
