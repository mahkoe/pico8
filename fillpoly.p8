--fillpoly

--global variables modified by
--scanpoly (and read by 
--fillpoly)
lts = {}
rts = {}
miny,maxy = 0,0

--helper function used by 
--scanpoly. not meant to be
--called by library user
function is_cw(pts)
	local dx1 = pts[2].x-pts[1].x
	local dy1 = pts[2].y-pts[1].y
	local dx2 = pts[3].x-pts[1].x
	local dy2 = pts[3].y-pts[1].y
	return dx1*dy2 > dx2*dy1
end

--helper function used by 
--scanpoly. not meant to be
--called by library user
function scanln(p1,p2,arr)
	local x = p1.x
	local dx = abs(p2.x-p1.x)
	local dy = abs(p2.y-p1.y)
	local n = 0
	local sy = 1
	if (p2.y < p1.y) sy = -1
	local sx = 1
	if (p2.x < p1.x) sx = -1
	local istep = sx*(dx\dy)
	local nstep = dx%dy
	for y = p1.y,p2.y,sy do
		arr[y] = x
		x += istep
		n += nstep
		if (n >= dy) then
			x += sx
			n -= dy
		end
	end
end

--helper function used by 
--scanpoly. not meant to be
--called by library user
function wrap(x,n)
	if (x > n) then
		x-=n
	elseif (x < 1) then
		x+=n
	end
	return x
end

--scan points of a polygon.
--doesn't draw anything; need
--to call fillpoly for that.
--instead, fills lts and rts
--arrays and sets miny,maxy
function scanpoly(pts)
	local n = #pts
	
	if (n<2 or is_cw(pts)) then
		miny,maxy = 0,0
		return
	end
	
	-- find index of min/max y
	local imin,imax = 1,1
	miny = pts[1].y
	maxy = pts[1].y
	
	for i=2,#pts do
		if (pts[i].y > maxy) then
			maxy = pts[i].y
			imax = i
		elseif (pts[i].y < miny) then
			miny = pts[i].y
			imin = i
		end
	end
	
	-- scan lines into lts or rts
	-- array (declared at top of
	-- this tab)
	local li,lf = imin,imax
	local ri,rf = imin,imax
	
	-- check special case for flat
	-- top/bottom
	local tmp = wrap(imin+1,n)
	if (pts[tmp].y==ymin) ri=tmp
	local tmp = wrap(imax+1,n)
	if (pts[tmp].y==ymax) lf=tmp
	
	-- scan left lines
	local idx = li
	local nxt = wrap(li+1,n)
	while (idx~=lf) do
		scanln(pts[idx],pts[nxt],lts)
		idx=nxt
		nxt=wrap(idx+1,n)
	end
	
	-- scan right lines
	idx = ri
	nxt = wrap(ri-1,n)
	while (idx~=rf) do
		scanln(pts[idx],pts[nxt],rts)
		idx=nxt
		nxt=wrap(idx-1,n)
	end
end

b_tbl = {
	0x0000, -- all dark
	0x0001, -- 1/16 light
	0x8020, -- 2/16 light
	0x080a, -- 3/16 light
	0x050a, -- 4/16 light
	0x5250, -- 5/16 light
	0x8525, -- 6/16 light
	0x25a5, -- 7/16 light
	0xa5a5, -- 8/16 light
	0xa5ad, -- 9/16 light
	0xada7, -- 10/16 light
	0xfada, -- 11/16 light
	0xfaf5, -- 12/16 light
	0x5f7f, -- 13/16 light
	0xdf7f, -- 14/16 light
	0xfeff, -- 15/16 light
	0xffff  -- all light
}

--lt: light colour idx
--dk: dark colour idx
--b: brightness, 1-17 inclusive
--assumes that you have already
--scanned the points of the
--polygon
function fillpoly(lt,dk,b)
	dk = dk or lt
	b = b or 9
	
	fillp(b_tbl[b])
	
	-- prevent overdraw for vert-
	-- ically adjacent polys by
	-- skipping first y
	local col = dk + 16*lt
	for y = miny+1,maxy do
		-- this if is bad for perf
		if (lts[y]<rts[y]) then
			line(
				lts[y]+1,y,
				rts[y],y,
				col
			)
		end
	end
end
