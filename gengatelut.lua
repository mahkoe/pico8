negators = {
	{0,0,0,0},
	{1,0,0,0},
	{0,1,0,0},
	{0,0,1,0},

	{0,0,0,1},
	{1,1,0,0},
	{0,1,1,0},
	{0,0,1,1},

	{1,0,0,1},
	{1,1,0,1},
	{1,1,1,0},
	{0,1,1,1},

	{1,0,1,1},
	{0,1,0,1},
	{1,0,1,0},
	{1,1,1,1},
}

function extract(n, field, width)
	width = width or 1
	local mask = (1<<width) - 1
	return (n>>field) & mask
end

io.write"{\n"
delim = " "
for i = 0,127 do
	local negs = negators[extract(i,3,4)+1]
	local p2 = extract(i,0)
	local p3 = extract(i,1)
	local p4 = extract(i,2)

	local val = (
		(p2 ~ negs[2]) |
		(p3 ~ negs[3]) |
		(p4 ~ negs[4])
	) ~ negs[1]

	io.write(delim, tostring(val))
	delim = ","
	if i%16 == 15 then
		delim = ",\n "
	end
end

io.write("\n}")
