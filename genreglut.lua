function extract(n, field, width)
	width = width or 1
	local mask = (1<<width) - 1
	return (n>>field) & mask
end

io.write"{\n"
delim = " "
for i = 0,3*64-1 do
	local dlen = extract(i,6,2)
	local state = extract(i,3,3)
	local p2 = extract(i,0)
	local p3 = extract(i,1)
	local p4 = extract(i,2)

	local latch = (p2|p4)>0
	local val = state
	if not latch then
		val = (val>>1) + (p3<<dlen)
	end

	io.write(delim, tostring(val))
	delim = ","
	if i%16 == 15 then
		delim = ",\n "
	end
end

io.write("\n}")
