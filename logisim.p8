pico-8 cartridge // http://www.pico-8.com
version 33
__lua__
-- game logic and globals
#include dbg.p8

-- goes true if an error is
-- found in postcontions()
pc_status = false

-- default grid object. acts
-- as a sentinel so we can
-- avoid explicit nil checks
blank = {
	tp = "blank", 
	state = 0,
	nocopy = true
}
def = {
	__index = function(t,k)
		return blank
	end
}

-- 1d array interpreted as
-- gw * gh boxes in row-major
grid = setmetatable({},def)
-- use 129 in most places, acts
-- as a sentinel so linear 
-- coords don't appear to wrap
-- around
gw   = 129 -- constant,"safe gw"
_gw  = 128 -- constant,"real gw"
gh   = 64  -- constant
nets = {}
gats = {}
jxns = {}

-- where last wire was placed.
-- dummy values so we  don't 
-- think we have a neighbour at 
-- the start.
lwx, lwy = -100,-100

-- cursor position. these are
-- absolute indices into the 
-- grid
cx,cy = 0,0
-- viewport into map
vx,vy = 0,0
-- determines what happens when
-- the user clicks 🅾️. can be:
-- - add wire
-- - add gate (or cycle through
--   negation state)
-- - add junction (or cycle 
--   through junction types)
-- - erase what's under the
--   cursor
mode = 1
modes = {
	"wire",
	"component",
	"erase",
	"interact"
}

-- state vars for component menu
gselx = 1
gsely = 1
gmenu = false
nrows,ncols = 7,4

-- number of frames per update
-- to state elements (0 for
-- single-step mode)
sim_sp = 1
sp_vals = {
	0, -- single-step mode
	1, -- default
	-- all other numbers are 
	-- frames/sim_tick
	2,5,10,20,50,
	100,200,500,
	1000
}
sp_idx = 2
function set_speed(b)
	if b&1>0 then
		-- reduce speed
		sp_idx = max(1,sp_idx-1)
	elseif b&2>0 then
		-- increase speed
		sp_idx=min(#sp_vals,sp_idx+1)
	end
	
	sim_sp = sp_vals[sp_idx]
	
	local sp_str = (
		(sim_sp==0)
		and "single"
		or tostr(sim_sp)
	)
	
	menuitem(
		1,
		"speed "..sp_str,
		set_speed
	)
end

undo_stack = {}
undo_pos = 0
crt = nil
skip_tick = false
undo_mode = false

menuitem(3,"save to disk", function()
	save_design()
end)

menuitem(4,"load from disk", function()
	load_design()
end)

menuitem(5,"copy area", function()
	copy_mode = true
	copy_state = "first"
end)

copy_mode = false
cpsel_x1,cpsel_x2 = 0,0
cpsel_y1,cpsel_y2 = 0,0
copy_state = "first"
copy_tdata = nil

function _init()
	set_speed(0)
	undo_mode = true
	toggle_undo_mode()
	-- set btnp delay and speed
	-- poke(0x5f5c, 3)
	-- poke(0x5f5d, 1)
	grid = setmetatable({},def)
	nets = {}
	gats = {}
	jxns = {}
	
	undo_stack = {
		[1] = {
			grid = {},
			nets = {},
			gats = {},
			jxns = {}
		}
	}
	undo_pos = 1
	
	-- generate lut for loading
	-- saved tile data
	gen_tinfo()
end

-- vars for dealing with cursor
-- movement
mvt_lut = {-1,1,0} --constant
mvt_lut[0] = 0     --constant
btn_d = 0
btn_dd = 0
fnum = 0
function _update()
	if sim_sp ~= 0 then
		fnum = fnum%sim_sp + 1
	else
		fnum=btnp()&0x30>0 and 1 or 0
		gmenu = false
	end
	
	if undo_mode then
		local u = btnp()&0x10>0
		local r = btnp()&0x20>0
		if u then
			do_undo(-1)
		elseif r then
			do_undo(1)
		end
	end

	if copy_mode then
		if btnp(🅾️) then
			if copy_state == "first" then
				cpsel_x1 = cx
				cpsel_y1 = cy
				copy_state = "second"
			else
				cpsel_x2 = cx
				cpsel_y2 = cy
				copy_tdata = copy_area(
					cpsel_x1, cpsel_y1,
					cpsel_x2, cpsel_y2
				)
				copy_mode = false
			end
		end
	end

	-- dumb log separator
	if btnp(➡️,1) then
		log"………………"
	end
	
 --btn bitfield is:
 --{❎🅾️⬇️⬆️➡️⬅️}	
	local val = band(
		btn(),
		bnot(bor(btn_d,btn_dd))
	)

	--local val = btnp()
	if gmenu then
		gselx += mvt_lut[band(val,3)]	
		gsely += mvt_lut[band(val,12)/4]
		gselx = min(max(gselx,1),ncols)
		gsely = min(max(gsely,1),nrows)
		-- ugly corner cases so we
		-- can't make invalid 
		-- selections
		gselx = min(
			gselx,
			gmenu_lens[gsely]
		)
	else
		cx += mvt_lut[band(val,3)]	
		cy += mvt_lut[band(val,12)/4]
		if cx < vx then
			vx = max(vx - 1, 0)
			cx = vx
		elseif cx > vx+15 then
			vx = min(vx + 1, _gw-16)
			cx = vx+15
		end
		
		if cy < vy then
			vy = max(vy - 1, 0)
			cy = vy
		elseif cy > vy+15 then
			vy = min(vy + 1, gh-16)
			cy = vy+15
		end
	end
	
	btn_dd = btn_d
	btn_d = btnp()
	
	-- continue copying state for
	-- the undo stack
	if 
		type(crt) == "thread" and
		costatus(crt) != "dead"
	then
		assert(costatus(crt) != "running")
		local dl = stat(1) + 0.1
		assert(coresume(crt, dl))
	end
	
	-- quit early if in single mode
	-- or undo mode
	if 
		undo_mode or
		sim_sp==0 or
		copy_mode
	then
		update_dbg()
		return
	end
	
	if btn(❎) then
		local updated = false
		if mode==1 then
			updated = place_wire(cx, cy)
		elseif 
			mode==2 and btnp(❎)
		then
		 if not gmenu then
		 	gmenu = true
		 else
		 	place_component(cx, cy)
		 	updated = true
		 	gmenu = false
		 	mode = 1 -- go to wire mode
		 end
		elseif mode==3 then
			updated = 
				erase_square(cx, cy)
		elseif mode==4 and btnp(❎) then
			local o = grid[cy*gw+cx]
			-- log(o.tp)
			if o.tp == "io" then
				o.state = 1 - o.state
			elseif o.tp == "reg" then
				local tno2len = {
					[80] = 1,
					[82] = 2,
					[86] = 3
				}
				local m = 1<<(
					tno2len[o.toff] -- so sue me
				)
				log("m = "..m)
				o.state = (o.state+1)%m
			end
		end
		
		if updated then
			crt = start_snapshot()
			local dl = stat(1)+0.1
			if type(crt)=="thread" then
				assert(costatus(ctr)=="suspended")
				coresume(crt,dl)
			end
		end
	end
	
	if btnp(🅾️) then
		mode = mode%#modes + 1
		gmenu = false
	end
	
	update_dbg()
end

last_pc_time = 0

function _draw()
 -------------------------------
 -- draw nets and cursors etc --
 -------------------------------
  
 --compute states for all nets--
 --so we draw them correctly  --
	update_nets()
	
	camera(8*vx, 8*vy)

	-- helper var
	local cl = cy*gw+cx
	-- background colour. make it
	-- pink if a postcondition is
	-- violated
	cls(pc_status and 14 or 15) 
	
	-- draw different colour for
	-- selected area in copy mode
	if 
		copy_mode and 
		copy_state == "second"
	then
		local x  = min(cpsel_x1,cx)
		local ex = max(cpsel_x1,cx)
		local y  = min(cpsel_y1,cy)
		local ey = max(cpsel_y1,cy)
		rectfill(
			x*8, y*8,
			ex*8+7, ey*8+7,
			9 -- orange
		)
	end
	
	-- draw the cursor
	spr(
		gmenu and 10 or 1, 
		cx*8, cy*8
	)
	
	-- draw helper stub to hint
	-- which way wire will connect
	-- (except during single step)
	if mode==1 and sim_sp>0 then
		draw_helper_stub(cl)
	end
	
	draw_grid(vx,vy)
	
	map(0,0,0,0,128,64)
	
	-- for some reason, even if
	-- the y coordinate is above
	-- the bottom of the screen
	-- due to the camera, the print
	-- funktion always scrolls the
	-- screen if the cursor is past
	-- 128. so reset camera when
	-- printing, i guess
	camera()
	if(gmenu and sim_sp>0) draw_gmenu()
	
	local mode_str = modes[mode]
	
	-- dumb assertion to prevent
	-- being in multiple special
	-- modes at the same time
	local mode_cnt = 0
	if(undo_mode)mode_cnt+=1
	if(sim_sp==0)mode_cnt+=1
	if(copy_mode)mode_cnt+=1
	assert(mode_cnt<2, "too many special modes")
	
	if undo_mode then
		mode_str = "undo/redo"
	elseif	sim_sp==0 then
		mode_str = "single"
	elseif copy_mode then
		mode_str = 
			"select "..copy_state.." corner"
	end
	draw_tool_text(mode_str)
	camera(vx,vy)
	
	-------------------------------
	-- update state of all delay --
	-- elements (includes gates) --
	-------------------------------
	if 
		fnum==1 and
		not skip_tick and
		not undo_mode
	then
	 update_state()
	end
	
	-------------------------
	-- debug/logging stuff --
	-------------------------
	
	if stat(0)+last_pc_time>0.99 then
		dbg"skipping postcon for perf"
	else
		dbg"=============="
		dbg"postconditions"
		dbg"=============="
		local tic = stat(0)
		postcondition()
		last_pc_time = stat(0) - tic
		dbg"=============="
	end
	
	dbg("num nets = "..#nets)
	dbg("num cmps = "..#gats)	
	dbg("num jxns = "..#jxns)
	
	-- dump_nets()
	
	-- debug stuff
	dbg("memory used: "..stat(0).." kb")
	print_dbg()
end
-->8
-- lookup tables

-- lookup tile number given a
-- binary state of "cardinal
-- directions". used to draw
-- wires based on neighbouring
-- connections, or for drawing
-- negators on gates
lut = {
	 0,  1,  2,  5,
	 3, 14,  6, 10,
	 4,  8, 13,  9,
	 7, 12, 11, 15
} --constant

-- little helper to simplify
-- the look of code that checks
-- neighbours of a grid tile
nbrs = {1,gw,-1,-gw}
nbrsw = {
	{  1, 1},
	{ gw, 2},
	{ -1, 4},
	{-gw, 8}
}
-- neighbours based on jtype
-- (only used in dfs, see there
-- for how we use it)
nbrsj = {
	[2] = {-1,-gw,1,gw},
	[6] = {-gw,-1,gw,1}
}

-- given self_linear_coord minus
-- neighbour_linear_coord, return
-- what port number it is on the
-- self
pnumlut = { --constant
	[-1]  = 1,
	[-gw] = 2,
	[1]   = 3,
	[gw]  = 4
}

-- order for drawing components
gord = setmetatable({
	{64, 77, 78, 79},
	{65, 66, 67, 68},
	{69, 70, 71, 72},
	{73, 74, 75, 76}
},def)
-- order for drawing junctions
jordan = setmetatable({
	[5] = {2,6}
},def)
-- order for drawing registers
reg = setmetatable({
	[6] = {96, 98, 102}
}, def)
-- order for drawing misc
thomas = setmetatable({
 [7] = {11,13}
}, def)
gmenu_lens = {4,4,4,4,2,3,2}


-- gate value lookup table.
-- select format is:
-- {negators, p4, p3, p2}
gatelut = {
 0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,
 1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,
 1,1,1,1,0,1,1,1,0,1,0,0,0,0,0,0,
 1,1,1,0,1,1,1,1,1,1,1,1,1,1,0,1,
 0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,
 0,0,0,1,0,0,0,0,1,1,1,1,1,1,1,0,
 0,0,0,0,0,0,1,0,1,1,1,1,1,0,1,1,
 0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1
}
gatelut.nocopy = true
-- reg value lookup table
-- select format is:
-- {
--   delay_len[1:0], 
--   state[2:0],
--   p4, p3, p2
-- }
reglut = {
 0,0,1,0,0,0,0,0,0,1,1,1,1,1,1,1,
 1,2,2,2,2,2,2,2,1,3,2,3,3,3,3,3,
 2,4,3,4,4,4,4,4,2,5,3,5,5,5,5,5,
 3,6,4,6,6,6,6,6,3,7,4,7,7,7,7,7,
 0,0,2,0,0,0,0,0,0,1,2,1,1,1,1,1,
 1,2,3,2,2,2,2,2,1,3,3,3,3,3,3,3,
 2,4,4,4,4,4,4,4,2,5,4,5,5,5,5,5,
 3,6,5,6,6,6,6,6,3,7,5,7,7,7,7,7,
 0,0,4,0,0,0,0,0,0,1,4,1,1,1,1,1,
 1,2,5,2,2,2,2,2,1,3,5,3,3,3,3,3,
 2,4,6,4,4,4,4,4,2,5,6,5,5,5,5,5,
 3,6,7,6,6,6,6,6,3,7,7,7,7,7,7,7
}
reglut.nocopy = true
-->8
-- helper functions

-- find all coords reachable
-- from linear coord l belonging
-- to net n. returns c,p,j
-- where c is a list of coords
-- belonging to n, and p is a 
-- list of gate ports connected
-- to n. j is a list of junction
-- net table object thingies.
-- another hack: if nlst is 
-- given, then use nlst as the
-- neighbours to traverse 
-- (o.w. just check all nbrs)
function dfs(l,n,nlst,seen,c,p,j)
	local seen = seen or {}
	p = p or {}
	j = j or {}
	if not c then
		c = {}
		
		if grid[l]==n then
			log(
				"⧗ add "..l..
				" ("..l%gw..
				","..l\gw..
				" to coord list"
			)
			add(c,l)
		elseif grid[l].tp=="jxn" then
			local dir2pnum = {
				[1] = 1,
				[gw] = 2,
				[-1] = 3,
				[-gw] = 4
			}
			assert(nlst[1])
			assert(#nlst == 1)
			local pnum = dir2pnum[nlst[1]]
			local p = grid[l].port[pnum]
			if p.net == n then
				add(c,l)
				add(j,p)
			end
		end
	end
	
	log(
		"dfs at "..
		l.." ("..
		l%gw..","..
		l\gw..")"
	)
	
	local nlst = nlst or nbrs
	
	for d in all(nlst) do
		-- log("> testing nbr "..d)
		local pnum_self = pnumlut[-d]
		local nl = l+d
		if not seen[nl] then
			if grid[nl]==n then
				log(
					"> nbr "..pnum_self..
					" is net"
				)
				log(
					"> add "..nl..
					" ("..nl%gw..
					","..nl\gw..
					" to coord list"
				)
				add(c,nl)
				seen[nl] = true
				dfs(nl,n,nbrs,seen,c,p,j)
			elseif grid[nl].is_comp then
				-- we are the neighbour of
				-- the gate, so look up
				-- pnumlut in opposite 
				-- direction
				local pnum_g = pnumlut[d]
				log(
					"> nbr "..pnum_self..
					" is gate"
				)
				local port=grid[nl].port[pnum_g]
				-- don't mark gate as seen
				-- because we could approach
				-- from other directions.
				-- safe becase no recursive
				-- call in this case
				if port.nbr==n then
					assert(n.g[port], "violated postcondition")
					log"> add to port list"
					add(p,port)
				end
			elseif grid[nl].tp=="jxn" then
				local pnum_j = pnumlut[d]
				assert(pnum_j)
				local o=grid[nl].port[pnum_j]
				log(
					"> nbr "..pnum_self..
					" is jxn"
				)
								
				if o.net==n then
					assert(n.j[o], "violated postcondition")
					-- add this to the list of
					-- junctions found by dfs
					add(j,o)
					-- also add this coordinate
					-- as part of the net
					add(c,o.parent.c)
				
					log"> jxn connected, keep dfsing"
					-- keep searching for
					-- neighbours starting from
					-- the junction square
					-- seen[nl] = true
					
					-- get list of neighbours
					-- to search 
					local nlst = {
						nbrsj[o.parent.jt][pnum_j]
					}
					assert(nlst[1])
					dfs(nl,n,nlst,seen,c,p,j)
				else
					log"this jxn is not connected"
				end
			else
				log"nothing to see here"
				seen[nl] = true
			end
		end
	end
	
	log"> dfs returning"
	
	return c,p,j
end

-- needle, haystack
function indexof(n,h) 
	for i,v in ipairs(h) do
		if v==n then
			return i
		end
	end
	
	return -1
end

function postcondition()
	dbg("start of postcon "..stat(1))
	----------
 -- nets --
 ----------
	for i,n in ipairs(nets) do
		-- check net <=> grid
		for c,_ in pairs(n.c) do
			if 
				(
					grid[c].tp!="jxn" and
					grid[c]!=n
				) or (
					grid[c].tp=="jxn" and
					grid[c].port[1].net!=n and
					grid[c].port[2].net!=n
				)
		 then
				dbg(
					"net "..i..
					" -> "..c..
					" but grid wrong"
				)
				pc_status = true
			end
		end
		-- check net <=> ports
		for p,_ in pairs(n.g) do
			if p.nbr != n then
				local g = p.parent
				local gnum = indexof(g,gats)
				dbg(
					"net "..i..
					" -> gate "..gnum..
					"."..tostr(p.num)..
					" but gate wrong"
				)
				pc_status = true
			end
		end
		-- check net <=> jxns
		for j,_ in pairs(n.j) do
			if j.net != n then
				local jj = j.parent
				local jnum = indexof(jj,jxns)
				dbg(
					"net "..i..
					" -> jxn "..jnum..
					"."..j.dir..
					" but jxn wrong"
				)
				pc_status = true
			end
		end
	end
	
	-----------
	-- gates --
	-----------
	for i,g in ipairs(gats) do
		-- check gate <=> grid
		if grid[g.c] != g then
			dbg(
				"gate "..i..
				" -> "..g.c..
				" ("..g.c%gw..
				","..g.c\gw..")"..
				" but grid wrong"
			)
				pc_status = true
		end
		
		-- check ports <=> neighbour
		for j = 1,4 do
			local p = g.port[j]
			local n = p.nbr
			if n.tp=="net" then
				local nnum=indexof(n,nets)
				if not n.g[p] then
					dbg(
						"gate "..i..
						"."..j..
						" -> net "..nnum..
						" but net wrong"
					)
					pc_status = true
				end
				if nnum < 0 then
					dbg(
						"gate "..i..
						"."..j..
						" -> unlisted net"
					)
					pc_status = true
				end
			elseif n.tp=="port" then
				if n.nbr != p then
					dbg(
						"gate "..i..
						" port "..j..
						" -> unrequited love"
					)
					pc_status = true
				end
			elseif n.tp != "blank" then
				dbg(
					"gate "..i..
					" port "..j..
					" -> unimp " .. tostr(n.tp)
				)
				pc_status = true
			end
		end
	end
	
	---------------
	-- junctions --
	---------------
	
	for i,j in ipairs(jxns) do
		-- check jxn <=> grid
		if grid[j.c] != j then
			dbg(
				"jxn "..i..
				" -> "..j.c..
				" ("..j.c%gw..
				","..j.c\gw..")"..
				" but grid wrong"
			)
			pc_status = true
		end
	
		-- check ports <=> neighbour
		local pr,pd=j.port[1],j.port[2]
		for p in all({pr,pd}) do
			local n = p.net
			assert(n ~= blank)
			local nnum = indexof(n,nets)
			if not n.j[p] then
				dbg(
					"jxn "..i..
					" port "..p.dir..
					" -> net "..
					nnum.. 
					" but net wrong"
				)
				pc_status = true
			end
			if nnum < 0 then
				dbg(
					"jxn "..i..
					"."..p.dir..
					" -> unlisted net"
				)
				pc_status = true
			end
		end
	end
	
	----------
	-- grid --
	----------
	for k,v in pairs(grid) do
		-- check grid <=> object
		-- there sholdn't actually
		-- be any pointers to blank
		-- objects (this is just the
		-- default return from the
		-- __index metamethod)
		if v.tp == "blank" then
			dbg("blank at grid "..k)
			pc_status = true
		elseif v.tp=="net" then
			local nnum = indexof(v,nets)
			if not v.c[k] then
				dbg(
					"grid "..k..
					" ("..k%gx..
					","..k\gw..
					") -> net "..nnum..
					" but net wrong"
				)
				pc_status = true
			end
			if nnum < 0 then
				dbg(
					"grid "..k..
					" ("..k%gw..
					","..k\gw..
					") -> unlisted net"
				)
				pc_status = true
			end
		elseif v.is_comp then
			local gnum = indexof(v,gats)
			if v.c ~= k then
				dbg(
					"grid "..k..
					" ("..k%gx..
					","..k\gw..
					") -> gate "..gnum..
					" but gate wrong"
				)
				pc_status = true
			end
			if gnum < 0 then
				dbg(
					"grid "..k..
					" ("..k%gx..
					","..k\gw..
					") -> unlisted gate"
				)
				pc_status = true
			end
		elseif v.tp=="jxn" then
			local jnum = indexof(v,jxns)
			if v.c ~= k then
				dbg(
					"grid "..k..
					" ("..k%gx..
					","..k\gw..
					") -> jxn "..jnum..
					" but jxn wrong"
				)
				pc_status = true
			end
			if jnum < 0 then
				dbg(
					"grid "..k..
					" ("..k%gx..
					","..k\gw..
					") -> unlisted jxn"
				)
				pc_status = true
			end
		end
	end
	
	dbg("end   of postcon "..stat(1))
end

function dump_nets()
	for i = 1,#nets do
		dbg("net "..i)
		local n = nets[i]
		for c,_ in pairs(n.c) do
			dbg(
				"> "..c.." ("..
				c%gw..","..c\gw..
				")"
			)
		end
		local nports = 0
		for p,_ in pairs(n.g) do
			nports += 1
		end
		local njxns = 0
		for j,_ in pairs(n.j) do
			njxns += 1
		end
		dbg("> num ports="..nports)
		dbg("> num jxns="..njxns)
		dbg("-------")
	end
end

function update_nets()
	--dbg"update drivers"
	for n in all(nets) do
		n.state = 0
	end
	for i,g in ipairs(gats) do
		--dbg("drive gate "..i)
		-- state has been computed
		-- on previous sim tick
		if g.state&1>0 then
			--port 1 is always output.
			--this may edit the state of
			--the blank object, but who
			--cares
			g.port[1].nbr.state = 1
		end
		g.port[1].state = g.state&1
	end
end

function update_state()
	-- dumb way to turn off dbg
	-- messages while i debug
	-- something else
	local dbg = function() end
	
	-- for convenience, set blank
	-- object's state low so we
	-- don't need to check for it
	-- explicitly
	blank.state = 0
	
	for i,g in ipairs(gats) do
		-- "lookup value"
		local luv = g.luv_base
		
		-- not all components have
		-- automatic state update
		if luv then
			luv += g.port[2].nbr.state
			luv += g.port[3].nbr.state*2
			luv += g.port[4].nbr.state*4
			
			if g.tp == "reg" then
				luv += g.state * 8
			end
			
			local dbg = dbg
			if (sim_sp == 0) then
				dbg = log
			end
			dbg("sim "..g.tp.." "..i)
			dbg("> p2 = "..g.port[2].nbr.state)
			dbg("> p3 = "..g.port[3].nbr.state)
			dbg("> p4 = "..g.port[4].nbr.state)
			dbg("> toff = "..g.toff)
			dbg("> state = "..g.state)
			dbg("> luv = "..luv)
			
			g.state = g.lut[luv]
		end
	end
end

function sim_tick()
	update_nets()
	update_state()
end

-- deep copy with a deadline.
-- you must give it a deadline
-- time. you must pass a table
-- for cpy; this can just be {}
-- if you don't care about it
function deepcpydl(t, d, cpy)
	assert(type(t) == "table")
	if stat(1) > d[1] then
		log"missed our deadline"
		d[1] = yield(false)
		log"resumed copy"
	end
	
	-- subtle: can still get
	-- infinite recursion without
	-- this
	local ret = {}
	assert(cpy[t]==nil)
	cpy[t] = ret
	
	for k,v in pairs(t) do
		key_is_t = false
		if type(k) == "table" then
			if not cpy[k] then
				if k.nocopy then
					cpy[k] = k
				else
					deepcpydl(k,d,cpy)
				end
				assert(cpy[k])
			end
			k = cpy[k]
		end
		
		if type(v) == "table" then
			if not cpy[v] then
				if v.nocopy then
					cpy[v] = v
				else
					deepcpydl(v,d,cpy)
				end
				assert(cpy[v])
			end
			v = cpy[v]
		end
		
		ret[k] = v
	end
	return ret
end

-- regular deep copy. must pass
-- table for cpy, can use {} if
-- you don't care about memoizing
function deepcpy(t, cpy)
	assert(type(t) == "table")
	-- subtle: can still get
	-- infinite recursion without
	-- this
	local ret = {}
	assert(cpy[t]==nil)
	cpy[t] = ret
	
	for k,v in pairs(t) do
		if type(k) == "table" then
			if not cpy[k] then
				if k.nocopy then
					cpy[k] = k
				else
					cpy[k] = deepcpy(k,cpy)
				end
				assert(cpy[k])
			end
			k = cpy[k]
		end
		
		if type(v) == "table" then
			if not cpy[v] then
				if v.nocopy then
					cpy[v] = v
				else
					cpy[v] = deepcpy(v,cpy)
				end
				assert(cpy[v])
			end
			v = cpy[v]
		end
		
		ret[k] = v
	end
	return ret
end
-->8
-- drawing functions

-- draw the 16x16 subgrid 
-- starting at ox,oy. w and h
-- let you override the width
-- and height of the drawn area,
-- default is 16x16
function draw_grid(ox,oy,w,h)
	local w = w or 16
	local h = h or 16
	-- weird hack that we shouldn't
	-- need: reset blank state to 0
	blank.state = 0
	
	dbg("start of draw_grid "..stat(1))
	for i = 0,h-1 do
		for j = 0,w-1 do
			local l = (oy+i)*gw + ox+j
			local o = grid[l]
			if o.tp == "net" then
				-- check neighbours to
				-- determine tile to draw
				local luv = 1
				for d in all(nbrsw) do
					local dl,w=unpack(d)
					local n = grid[l+dl]
					
					if n==o then
						luv += w
					elseif n.port then
						local pnum = pnumlut[dl]
						if 
							n.port[pnum].nbr==o or
							n.port[pnum].net==o
						then
							--FIXME: try to fix this
							--ugly nbr/net thing
							luv += w
						end
					end
				end
				
				local off = (
					o.state==1 and 32 or 16
				)
				
				local tno = off + lut[luv]
				mset(ox+j,oy+i,tno)
			elseif o.tp == "gate" then
				local off = 
					o.state==1 and 64 or 48
				;
				local tno = off + o.toff
				mset(ox+j,oy+i,tno)
			elseif o.tp == "jxn" then
				local rs = o.port[1].net.state
				local ds = o.port[2].net.state
				local tno = o.jt + 2*ds + rs
				mset(ox+j,oy+i,tno)
			elseif o.tp == "reg" then
				local latched = (
					o.port[2].nbr.state |
					o.port[4].nbr.state
				)
				-- dbg("⌂ o.state = "..tostr(o.state))
				local tno = (
					o.toff + 
					latched*16 +
					o.state
				)
				mset(ox+j,oy+i,tno)
			elseif o.tp == "io" then
				local tno = o.toff + o.state
				mset(ox+j,oy+i,tno)
		 else
				mset(ox+j,oy+i,0)
			end
		end
	end
	
	dbg("end   of draw_grid "..stat(1))
end

function draw_gmenu()
	local tlx,tly = 8,8
	if(cx<8) tlx = 127-10*(ncols+1)
	if(cy<8) tly = 127-10*(nrows+1)
	
	rect(
		tlx, tly,
		tlx+10*ncols+1, 
		tly+10*nrows+1,
		1
	)
	rectfill(
		tlx+1, tly+1,
		tlx+10*ncols, 
		tly+10*nrows,
		7
	)
	
	-- just make everything red
	pal(2,8)
	for i = 1,nrows do
		for j = 1,ncols do
			local t = (
				gord[i][j] or 
				jordan[i][j] or
				reg[i][j] or
				thomas[i][j]
			)
			if t then
				spr(
					t,
					tlx-8+10*j,
					tly-8+10*i
				)
			end
		end
	end
	-- reset palette
	pal(2,2)
	
	spr(
		1,
		tlx-8+10*gselx,
		tly-8+10*gsely
	)
end

function draw_helper_stub(cl)
		local lwl = lwy*gw+lwx
		local d = cl - lwl
		local tlut = {
			[1] = 35,
			[gw] = 36,
			[-1] = 33,
			[-gw] = 34
		}
		
		local tno = tlut[d]
		if tno then
			pal(8,14)
			spr(tno,cx*8,cy*8)
			spr(tlut[-d],lwx*8,lwy*8)
			pal(8,8)
		end
end

-- expects camera to be at 0,0
function draw_tool_text(str)
	local txt_w = 4*#str
	local txt_h = 5
	local txt_x = 126-txt_w
	local txt_y = 2
	--dbg("tx,ty = "..txt_x..","..txt_y)
	rectfill(
		txt_x-1,txt_y-1,
		txt_x+txt_w-1,
		txt_y+txt_h,
		15
	)
	cursor(txt_x,txt_y)
	print(str,1)
end
-->8
-- data structure mgmt


function place_wire(cx, cy)
	-- dumb way to turn off log
	-- messages while i debug
	-- something else
	local log = function() end

 -- what is currently in this
 -- square of the grid
 
 -- l for "linear coord"
 local l = cy*gw+cx
	local lwl = lwy*gw+lwx
	local c = grid[l]
	
	-- what is pointed to by the
	-- last wire we placed
	local p = grid[lwy*gw+lwx]
	
	-- if c and/or p are junctions,
	-- decay them to the relevant
	-- net
	if p.tp == "jxn" then
		local d = lwl-l
		local pnum = pnumlut[d] or 1
		p = p.port[pnum].net
	end
	
	local parent_jxn = false
	if c.tp == "jxn" then
		parent_jxn = c
		local d = l-lwl
		local pnum = pnumlut[d] or 1
		c = c.port[pnum].net
	end
	
	local p_is_adj =
		((abs(cx-lwx)+abs(cy-lwy))==1)
	;
	local p_is_net_n = 
		p_is_adj and
  (p.tp == "net")
	;
	local p_is_gate_n = 
		p_is_adj and
  p.is_comp
	;
	
	-- update last placed wire pos
	lwx,lwy=cx,cy
	
	-- ugly hack: to prevent
	-- writing a bunch of extra
	-- cases, swap p and c if it
	-- lets us use an existing
	-- code path
	local s = 1
	if 
		c.tp=="net" and 
		p_is_gate_n
	then
		log"swapping p and c"
		local tmp = c
		c = p
		p = tmp
		p_is_net_n = true
		p_is_gate_n = false
		s = -1
	end
	
	if c.tp=="net" then 
		--log"wire on wire"
		if p_is_net_n then
			log"merge prev and cur"
			if c != p then
			 -- for each of c's coords,
			 -- edit them in the grid to
			 -- point to p. also add them
			 -- into p's coord list
			 for ll,_ in pairs(c.c) do
			 	grid[ll] = p
			 	p.c[ll] = true
			 end
			 
				-- add all of c's gates to p
			 -- and make sure the port 
			 -- pointers are updated
			 for g,_ in pairs(c.g) do
			 	p.g[g] = true
			 	assert(g.nbr == c)
		 		g.nbr = p
			 end
			 
			 -- add all of c's jxns to p
			 -- and make sure the net
			 -- pointers are updated.
			 for j,_ in pairs(c.j) do
			 	if(p.j[j])log"joining jxn to itself"
			 	p.j[j] = true
			 	j.net = p
				 -- hack: because the for
				 -- loop for the nets 
				 -- clobbered all the grid
				 -- squares, replace the
				 -- junctions
			 	grid[j.parent.c] = j.parent
			 end
				
				-- delete c from nets list
				del(nets, c)
			end
		else
			--log"no work"
			return false
		end
	elseif c.is_comp then
		log"wire on gate/reg"
		if p_is_net_n then
			log"connect port"
			-- find port object that
			-- aims from c to lw
			local d = s*(l - lwl)
			local pnum = pnumlut[d]
			--pnum can be nil if the
			--last wire is far away
			if pnum then
				--connect net to port
				local pt = c.port[pnum]
				if pt.nbr == p then
					log"already connected"
					return false
				end
				pt.nbr = p
				p.g[pt] = true
			end
		else
			log"nothing to do net2gate"
			return false
		end
	else
		-- check if prev is neighbour
		if p_is_net_n then
			log"add to previous wire"
			p.c[l] = true
			grid[l] = p
		else
			log"create new net"
			local n = {
				tp = "net",
				g = {},
				j = {},
				state = 0,
				c = {[l] = true}
			}
			add(nets,n)
			grid[l] = n
			if p_is_gate_n then
				log"connect new net to port"
				local d = s*(lwl-l)
				local pnum = pnumlut[d]
				local port = p.port[pnum]
				n.g[port] = true
				port.nbr = n
			end
		end
	end
	
	return true
end

function erase_square(cx,cy,d)
	if(not d)log"erase_square"
	local d = d or 0
	if d > 100 then
		log"infinite recursion"
		return
	end
 -- "linear coord"
	local l = cy*gw+cx
	local c = grid[l]
	
	if c.tp == "blank" then
		--log"tried to erase blank"
		return false --nothing changed
	elseif c.is_comp then
		-- for each port, follow
		-- it and disconnect
		for i = 1,4 do
			local p = c.port[i]
			if p.nbr.tp=="net" then
				p.nbr.g[p] = nil
			elseif p.nbr.tp=="port" then
				--this case covers both
				--components and junctions
				p.nbr.nbr = blank
			end
		end
		-- erase from map & gates list
		del(gats,c)
		grid[l] = nil
	elseif c.tp == "jxn" then
		del(jxns, c)
		local nr = c.port[1].net
		assert(nr ~= blank)
		grid[l] = nr
		erase_square(cx, cy, d+1)
		local nd = c.port[2].net
		assert(nd ~= blank)
		if nd ~= nr then
			grid[l] = nd
			erase_square(cx, cy, d+1)
		end
	else
		-- remove this coord from the
		-- net, and clear the grid too
		assert(c.c[l])
		c.c[l] = nil
		grid[l] = nil
		
		-- remove net from nets list. 
		-- the remaining portions will
		-- be re-added below
		del(nets, c)
			
		if not next(c.c) then
			log"deleted last coord of net"
			-- edit all connected ports
			-- to point to blank
			for p,_ in pairs(c.g) do
				p.nbr = blank
			end
			for j,_ in pairs(c.j) do
				j.net = blank
			end
			return
		end
		log"did not delete last coord of net"

		
		for d in all(nbrs) do
			local nbr = grid[l+d]
			-- if nbr is a jxn, decay
			-- it to the underlying net
			local nlst = nil
			if nbr.tp=="jxn" then
				log"found jxn neighbour"
				local pnum = pnumlut[d]
				log(
					" port "..pnum..
					" on neighbour"
				)
				if nbr.port[pnum].net==c then
					log" decaying ptr"
					nlst = {
						nbrsj[nbr.jt][pnum]
					}
					assert(nlst[1])
					nbr = nbr.port[pnum].net
				end
			end
			
			-- if nbr is a gate, then
			-- this is a corner case to
			-- clear the connection on
			-- that gate port
			if nbr.is_comp then
				log"found comp neighbour"
				local pnum = pnumlut[d]
				log(
					" port "..pnum..
					" on neighbour"
				)
				if nbr.port[pnum].nbr==c then
					log" clearing"
					nbr.port[pnum].nbr = blank
				end
			elseif nbr == c then
				log"found net neighbour"
				-- run dfs and create new
				-- net from result
				local r,gp,jp = dfs(l+d,c,nlst)
				
				log(
					"dfs returned "..#jp..
					" jxns, "..#gp.." gates"
				)
				
				--make new net from r (and
				--returned gates/jxns)
				local nn = {
					tp="net", 
					state = c.state,
					c={}, g={}, j={}
				}
				add(nets,nn)
				for ll in all(r) do
					grid[ll] = nn
					nn.c[ll] = true
				end
				for pp in all(gp) do
					nn.g[pp] = true
					pp.nbr = nn
				end
				for jj in all(jp) do
					-- dirty hack: because the
					-- nets loop clobbered the
					-- junctions, put them
					-- back on the grid
					nn.j[jj] = true
					jj.net = nn
					grid[jj.parent.c]=jj.parent
				end
			end
		end
	end
	
	return true
end

-- o is the component object,
-- lst is list to put it in
function place_obj(cx,cy,o)
	local l = cy*gw+cx
	
	o.is_comp = true
	o.c = l
	
	-- erase whatever was here
	-- before
	erase_square(cx, cy)
	
	o.port = {}
	for i = 1,4 do
		o.port[i] = {
			tp = "port",
			num = i, -- needed?
			state = 0,
			parent = o,
			nbr = blank
		}
		-- auto-connect to adjacent
		-- neighbouring gates/jxns
		local d = nbrs[i]
		local n = grid[l+d]
		if n.is_comp then
			local pnum_n = pnumlut[d]
			local pnum_g = pnumlut[-d]
			assert(pnum_g==i)
			n.port[pnum_n].nbr= (
				o.port[pnum_g]
			)
			o.port[pnum_g].nbr = (
				n.port[pnum_n]
			)
		elseif n.tp=="jxn" then
			local pnum_n = pnumlut[d]
			local pnum_g = pnumlut[-d]
			assert(pnum_g==i)
			--neighbour net
			local nn = n.port[pnum_n].net
			local gport = o.port[pnum_g]
			nn.g[gport]=true
			gport.nbr = nn
		end
	end
	
	add(gats,o)
	grid[l] = o
end

-- jt is junction tile
function place_jxn(cx,cy,jt)
	local l = cy*gw+cx
	
	-- erase whatever was here
	-- before
	erase_square(cx, cy)
	
	-- figure out if this is a
	-- flyover or abutting junction

	local is_flyover = (jt==2)

	-- create two new nets for 
	-- this junction
	local nr = {
		tp = "net",
		state = 0,
		c = {[l] = true},
		g = {},
		j = {}
	}
	
	local nd= {
		tp = "net",
		state = 0,
		c = {[l] = true},
		g = {},
		j = {}
	}
	
	-- create a new junction table
	local j = {
		tp = "jxn",
		jt = jt,
		c = l,
		port = {
			{tp="port",net=nr,dir="r"},
			{tp="port",net=nd,dir="d"}
		}
	}
	
	local ns = j.port
	
	if is_flyover then
		ns[3] = ns[1]
		ns[4] = ns[2]
	else
		ns[4] = ns[1]
		ns[3] = ns[2]
	end
	
	ns[1].parent = j
	ns[2].parent = j
	
	nr.j[ns[1]] = true
	nd.j[ns[2]] = true
	
	add(nets, nr)
	add(nets, nd)
	
	grid[l] = j
	add(jxns, j)
end

function place_component(cx,cy)
	-- check gsel and delegate to
	-- correct place_x function
	
	local g = gord[gsely][gselx]
	local j = jordan[gsely][gselx]
	local r = reg[gsely][gselx]
	local t = thomas[gsely][gselx]
	if g then
		-- tile offset 
		local gt = g-64
			
		local o = {
			tp = "gate",
			toff = gt,
			luv_base = (
				--gatelut uses negators as
				--bits [6:3] of select. but
				--in lua we need to add 1
				--for 1-indexing. precompute
				--some of this here to save
				--effort when simluating
				gt*8+1
			),
			lut = gatelut,
			state = 0
		}
		place_obj(cx,cy,o)
	elseif j then
		place_jxn(cx, cy, j)
	elseif r then
		local tno2len = {
			[96] = 1,
			[98] = 2,
			[102] = 3
		}
		assert(tno2len[r])
		local o = {
			tp = "reg",
			toff = r-16,
			luv_base=1+(tno2len[r]-1)*64,
			lut = reglut,
			state = 0
		}
		place_obj(cx,cy,o)
	elseif t then
		-- spaghetti ahoy
		if t == 11 then
			-- io component
			local o = {
				tp = "io",
				toff = t,
				state = 0
			}
			place_obj(cx,cy,o)
		else
			-- paste
			paste_area(cx,cy,copy_tdata)
		end
	else
		log"tried to place nothing"
	end
end
-->8
-- save/load/copy/paste

-- prevent needing explicit
-- nil checks
tinfo = setmetatable({},def)

function gen_tinfo()
	-- jxns
	for i = 2,5 do
		--flyover
		tinfo[i] = {
			tp = "jxn",
			jt = 2
		}
		--abutting
		tinfo[i+4] = {
			tp = "jxn",
			jt = 6
		}
	end
	
	-- ios
	for i = 11,12 do
		tinfo[i] = {
			tp = "io",
			toff = 11,
			state = i - 11,
			is_comp = true
		}
	end
	
	-- gates
	for i = 48,63 do
		local gt = i - 48
		tinfo[i] = {
			tp = "gate",
			toff = gt,
			luv_base = gt*8+1,
			lut = gatelut,
			state = 0,
			is_comp = true
		}
		tinfo[i+16] = {
			tp = "gate",
			toff = gt,
			luv_base = gt*8+1,
			lut = gatelut,
			state = 1,
			is_comp = true
		}
	end
	
	-- regs
	-- len = 1
	for i = 80,81 do
		tinfo[i] = {
			tp = "reg",
			toff = 80,
			luv_base = 1,
			lut = reglut,
			state = i-80,
			is_comp = true
		}
		tinfo[i+16] = {
			tp = "reg",
			toff = 80,
			luv_base = 1,
			lut = reglut,
			state = i-80,
			is_comp = true
		}
	end
	-- len = 2
	for i = 82,85 do
		tinfo[i] = {
			tp = "reg",
			toff = 82,
			luv_base = 65,
			lut = reglut,
			state = i-82,
			is_comp = true
		}
		tinfo[i+16] = {
			tp = "reg",
			toff = 82,
			luv_base = 65,
			lut = reglut,
			state = i-82,
			is_comp = true
		}
	end
	-- len = 3
	for i = 86,93 do
		tinfo[i] = {
			tp = "reg",
			toff = 86,
			luv_base = 129,
			lut = reglut,
			state = i-86,
			is_comp = true
		}
		tinfo[i+16] = {
			tp = "reg",
			toff = 86,
			luv_base = 129,
			lut = reglut,
			state = i-86,
			is_comp = true
		}
	end
	
	-- nets
	local connected_dirs = {
		{}, 
		{1}, {gw}, {-1}, {-gw},
		{1,gw},{gw,-1},{-1,-gw},{-gw,1},
		{-gw,1,gw},{1,gw,-1},{gw,-1,-gw},{-1,-gw,1},
		{-gw,gw},{-1,1},
		{1,gw,-1,-gw}
	}
	for i = 16,31 do
		tinfo[i] = {
			tp = "net",
			dirs = connected_dirs[i-15]
		}
		tinfo[i+16] = {
			tp = "net",
			dirs = connected_dirs[i-15]
		}
	end
	
	-- part of our ugly hack to
	-- squeeze more info into the
	-- tile data
	for i = 128,255 do
		tinfo[i] = tinfo[i-128]
	end
end

-- give nonzero ox,oy to paste
-- at different position. if you
-- pass in tdata, the function 
-- will read from there instead.
-- the tdata should be a table
-- of tables indexed using
-- tdata[y][x]. it is safe for
-- elements of tdata to be nil
function scan_tiles(x,y,ex,ey,ox,oy,tdata)
	local x = x or 0
	local y = y or 0
	local ex = ex or 127
	local ey = ey or 63
	local ox = ox or x
	local oy = oy or y
	
	local mget = mget
	
	if tdata then
		mget = function(x,y)
			local row = tdata[y]
			if(not row) return 0
			local tno = row[x]
			return tno or 0
		end
	end
	
	-- first pass, deal with
	-- components
	for i = y,ey do
		for j = x,ex do
			local tno = mget(j,i)
			local ti = tinfo[tno]
			-- if component or junction,
			-- then place it.
			if ti.is_comp then
				local o = deepcpy(ti,{})
				place_obj(j+ox,i+oy,o)
			elseif ti.tp == "jxn" then
				log"found jxn"
				place_jxn(j+ox,i+oy,ti.jt)
			end
		end
	end
	
	-- TODO: clean this up
	local dxlut = {
		[-1]  = -1,
		[-gw] = 0,
		[1]   = 1,
		[gw]  = 0
	}
	local dylut = {
		[-1]  = 0,
		[-gw] = -1,
		[1]   = 0,
		[gw]  = 1
	}
	
	-- second pass, deal with nets
	for i = y,ey do
		for j = x,ex do
			local tno = mget(j,i)
			local ti = tinfo[tno]
			
			if ti.tp == "net" then
				-- ugly... reset lwx and lwy
				-- to prevent unexpected
				-- connections
				lwy = -100
				place_wire(j+ox,i+oy)
				for d in all(ti.dirs) do
					local dx = dxlut[d]
					local dy = dylut[d]
					place_wire(j+ox+dx,i+oy+dy)
					place_wire(j+ox,i+oy)
				end
			end
		end
	end
end

function save_design(fname)
	draw_grid(0,0,128,64)
	cstore(0x1000,0x1000,0x2000,fname)
end

function load_design(fname)
	_init()
	reload(0x1000,0x1000,0x2000,fname)
	scan_tiles()
end

-- returns a 2d array suitable
-- for passing into tdata arg
-- of scan_tiles
function copy_area(x,y,ex,ey)
	
	-- enforce ex >= x
	if ex<x then
		local tmp = x
		x = ex
		ex = tmp
	end
	
	-- enforce ey >= y
	if ey<y then
		local tmp = y
		y = ey
		ey = tmp
	end
	
	local ret = {}
	
	for i = y,ey do
		ret[i-y] = {}
		for j = x,ex do 
			ret[i-y][j-x] = mget(j,i)
		end
	end
	
	ret.dx = ex - x
	ret.dy = ey - y
	
	return ret
end

function paste_area(x,y,tdata)
	--cover our butts
	if 
		not tdata or
		type(tdata) != "table" or
		not tdata.dx or
		not tdata.dy
	then
		log"this copy buffer is empty"
		return
	end

	local ex = tdata.dx
	local ey = tdata.dy
	
	-- prevent pasting outside grid
	-- boundary. technically this
	-- is safe, but it wastes mem
	ex = min(ex,_gw-x-1)
	ey = min(ey,gh-y-1)
	
	scan_tiles(
		0,0,   -- tdata starts at 0,0
		ex,ey, -- size of tdata
		x,y,   -- where to place
		tdata
	)
end
-->8
-- undo stack

function start_snapshot()
	skip_tick = false
	-- deep copy everything. we'll
	-- figure out pointer updates
	-- later
	local thing_to_do = function(dl)
		while 
			stat(0) > 1536 and
			#undo_stack > 0
		do
			log"need to cull old undoes"
			deli(undo_stack,1)
			undo_pos -= 1
		end
		
		--log"started copy"
		local cpy = {}
		local s = {}
		-- wrap deadline in a table so
		-- function calls can edit it
		-- ("pass by reference")
		local d = {dl}
		-- must do components first
		-- because nets point to comps
		-- and we might deep copy them
		-- early
		--log"copy components"
		skip_tick = true
		s.gats = deepcpydl(gats,d,cpy)
		skip_tick = false
		--log"copy nets"
		s.nets = deepcpydl(nets,d,cpy)
		--log"copy jxns"
		s.jxns = deepcpydl(jxns,d,cpy)
		--log"copy grid"
		s.grid = deepcpydl(grid,d,cpy)
		
		for i = undo_pos+1,#undo_stack do
			log("deleting undo entry "..i)
			undo_stack[i] = nil
		end
				
		undo_pos += 1
		log("creating undo entry "..undo_pos)
		undo_stack[undo_pos] = s
		return true
	end
	
	return cocreate(thing_to_do)
end

function do_undo(off)
	local len = #undo_stack
	-- log("#undoes = "..len)
	log("undo_pos = "..undo_pos)
	
	local n = undo_pos + off
	if not undo_stack[n] then
		log"cannot undo/redo"
		return
	end
	
	log"time to undo!"
	
	local cpy = undo_stack[n]
	
	local memo = {}
	grid = setmetatable(
		deepcpy(cpy.grid, memo),
		def
	)
	nets = deepcpy(cpy.nets, memo)
	gats = deepcpy(cpy.gats, memo)
	jxns = deepcpy(cpy.jxns, memo)
	
	undo_pos = n
	log("new undo pos = "..undo_pos)
end

function toggle_undo_mode()
	if not undo_mode then
		-- entering undo mode;
		-- let any running copies
		-- finish
		if type(crt) == "thread" then
			assert(costatus(crt)!="running")
			while costatus(crt)!="dead" do
				coresume(crt,5)
			end
		end
	end

	undo_mode = not undo_mode
	menuitem(
		2,
		undo_mode 
		  and "leave undo/redo"
		  or "undo/redo mode"
		,
		toggle_undo_mode
	)
end
-->8
-- notes

-- sfx names
--[[
7 = even kick (k_c_)
8 = odd kick  (_i_k)
9 = takakachikataka    + kc
10 = lead bar 1        + ik 
11 = lead bar 2 
12 = lower voice bar 2 + ik
13 = lead bar 2        + ik
14 = waaooaa pickup
15 = waaooaa bar 1
16 = weeooee pickup
17 = weeooee bar 1
18 = waaooaa bar 2
19 = weeooee bar 2
]]

--
-- FIXME? jxns should auto-
-- connect?
--
-- TODO: make sure to create a
-- snapshot when pasting
-- TODO: erase squares before
-- pasting
-- FIXME: pasting was wraparound
-- problem
-- FIXME: oom problems 😐
--
-- [x] implement scrolling
--     [x] fix text placement
--     [x] fix wraparound
--     [x] fix not being able to
--         to draw in bottom 1/2
-- [x] back-to-back-gates
-- [x] junctions
--     [x] actually connect to
--         nets properly
--     [x] fix problems with
--         erasing and dfs
--     [x] fix gates connecting
--         automatically
--     [x] fix corner case where
--         junction only has one
--         net left
-- [x] register elements
-- [x] control sim speed
-- [x] undo stack
-- [/] i/o components
-- [x] save+load support
--     [x] deserializing tile
--         list into net and
--         component objs
-- [ ] copy+paste
-- [ ] that thing about toggling
--     connections from nets to
--     gates
-- [ ] better tool select
--     - hold z and hit an arrow
--       key
--     - or anything that seems
--       better
--
-- for new component types, 
-- merge them into the gate
-- code path. junctions will
-- pretty much always need their
-- own code path. 
--
--
-- other stuff:
-- - we need things the circuit
--   controls. maybe there's an
--   opportunity with sound?
-- - or that lunar lander idea
-- - line follower car thing
-- - have challenges (like
--   shenzhen i/o)
-- - can we do multi-bit buses?
-- - can we do analog like in 
--   redstone? rules:
--   - not: out=max_num - in
--   - or:  out=max(in0,in1,...)
-- - what to do for music? need
--   2-3 songs to loop for 
--   ambience
--   - jango mahjong ost!
-- - waveform viewer would rock
-- - text comments
-- - built-in mux primitive
-- - built-in xor primitive
--   - possible to make xor from
--     mux

-- sim loop:
-- - for every driver (currently
--   this only includes gates)
--   set driven net's state to
--   high. note: if multiple
--   drivers connect to same
--   net, the net's value is the
--   or of all the drivers
-- - draw the scene, taking care
--   to highlight stuff properly
-- - compute next_state for all
--   storage elements. currently
--   this is just gates, and 
--   they only have one cycle of
--   storage.


-- big comment that used to be
-- in tab 1

-- for each coord, it points to
-- either nil or a net, gate, or
-- junction object
--
-- - a net object has a list of
--   connected gates. maybe 
--   should also list junctions
--   for convenience.
-- 
-- - a gate object has a next 
--   value that will be applied
--   on the next sim tick
--
-- - a junction object has a 
--   list of the two nets it
--   connects to. we need the
--   values of the nets to know
--   how to draw the junction.
--   - two kinds of junction
--     objects: flyover and
--     abutting
--
-- - anytime "thing a" can 
--   "point" to "thing b", we
--   should also make sure that
--   "thing b" points back to
--   "thing a". the grid array
--   has pointers to objects,
--   but each object should also
--   keep a list of grid coords.
--   and if, for example, a net
--   points to a gate, the gate
--   should point to the net.
--   we can add an assertion to 
--   make sure everything stays 
--   in sync
--
-- on every frame:
-- 1. apply current state of
--    each gate to all nets
--    (using rule that multiple
--    drivers or together)
-- 2. draw visible region of map
--    - or draw everything if
--      we're saving on this
--      frame
-- 3. compute next state for all
--    gates



-- ancient stuff
--[[
	-- test dfs
	--[[
	if #nets > 0 then
		local n = nets[1]
		local c = next(n.c)
		dbg("seed "..c)
		local r = dfs(c,n)
		add(r,c)
		--dbg(r)
		local rs = {}
		for l in all(r) do
			assert(n.c[l])
			rs[l] = true
		end 
		
		for l,_ in pairs(n.c) do
			assert(rs[l])
		end
	end

	-- dump nets
	

	
	-- dump gates
	for i,g in ipairs(gats) do
		dbg("gate "..i)
		local c = g.c
		dbg(
			"> "..c.." ("..
			c\gw..","..c%gw..
			")"
		)
		dbg("> state = "..g.state)
	end

]]


-- from somewhere in erase_square

				--[[
				for ll in all(r) do
					log("> "..ll)
				end
				
				for port in all(gp) do
					local gnum = indexof(
						port.parent, gats
					)
					log(
						"> gate "..gnum..
						"."..port.num
					)
				end
				]]--
				
-- reject serialization ideas

-- serialization
--
-- note: jxns not serialized;
-- we will infer them from
-- cases where nets overlap
-- 
--[[
================
== data types ==
================

moveto command (2 bytes)
- coord

element
- type 7 bits
- relative x 1 bit
  (dx is this value + 1)

type:
  enum between net,gate,reg,jxn.
  right now needs 2 bits but we
  reserve 

coordinates:
  a coord indicates one square
  of a 64x128 grid, so we only
  need 6+7 = 13 bits for it
  
net path:
  we can save space when s'zing
  nets by using the fact they
  are connected. a net will be
  described as the traversal of
  a tree. 
  
  the first part of the
  traversal is the x and y 
  coords of the root (1b each)
  
  hen, every element of the
  traversal has format:
  
  bit 3:
  - if 1, this is a parent link
  - if 0, this is a child or
    sibling
  
  if bit 3 is 1, we read bits
  [2:0] as the number of parents
  to "unwind" minus 1. for ex,
  0b1000 means "up one parent",
  0b1101 means "up six parents".
  if you pass the original root,
  then the traversal ends
  
  if bit 3 is 0, we read bit 2.
  if bit 2 is 1 this is a child,
  else a sibling. then, bits 1:0
  indicate the direction in the
  grid between the two nodes

]]--

--[[tp2num = {
	net = 0,
	gate = 1,
	reg = 2,
	jxn = 3
}]]


__gfx__
00000000ccc00ccc0002200000022000000880000008800000022000000880000002200000088000444004440000000000000000455555400000000000000000
00000000cc0000cc0002200000022000000880000008800000022000000880000002200000088000440000440222222008888880477777400000000000000000
00700700c000000c0002200000022000000880000008800000022200000888000002220000088800400000040211122008818880471117400000000000000000
00077000000000002202202288022088220880228808808822202222222088888880222288808888000000000212122208818888471717400000000000000000
00077000000000002202202288022088220880228808808822220222222208888888022288880888000000000212122208818888471117400000000000000000
00700700c000000c0002200000022000000880000008800000222000002220000088800000888000400000040211122008818880471777400000000000000000
00000000cc0000cc0002200000022000000880000008800000022000000220000008800000088000440000440222222008888880477777400000000000000000
00000000ccc00ccc0002200000022000000880000008800000022000000220000008800000088000444004440000000000000000444444400000000000000000
00000000000000000000000000000000000220000000000000000000000220000002200000022000000000000002200000022000000220000000000000022000
00000000000000000000000000000000000220000000000000000000000220000002200000022000000000000002200000022000000220000000000000022000
00000000000000000000000000000000000220000000000000000000000220000002200000022200000000000022200000222200000220000000000000222200
00022000000222220002200022222000000220000000222222220000222220000002222200002222222002222222000022222222000220002222222222222222
00022000000222220002200022222000000220000002222222222000222200000000222200002222222222222222000022200222000220002222222222222222
00000000000000000002200000000000000000000002200000022000000000000000000000022200002222000022200000000000000220000000000000222200
00000000000000000002200000000000000000000002200000022000000000000000000000022000000220000002200000000000000220000000000000022000
00000000000000000002200000000000000000000002200000022000000000000000000000022000000220000002200000000000000220000000000000022000
00000000000000000000000000000000000880000000000000000000000880000008800000088000000000000008800000088000000880000000000000088000
00000000000000000000000000000000000880000000000000000000000880000008800000088000000000000008800000088000000880000000000000088000
00000000000000000000000000000000000880000000000000000000000880000008800000088800000000000088800000888800000880000000000000888800
00088000000888880008800088888000000880000000888888880000888880000008888800008888888008888888000088888888000880008888888888888888
00088000000888880008800088888000000880000008888888888000888800000000888800008888888888888888000088800888000880008888888888888888
00000000000000000008800000000000000000000008800000088000000000000000000000088800008888000088800000000000000880000000000000888800
00000000000000000008800000000000000000000008800000088000000000000000000000088000000880000008800000000000000880000000000000088000
00000000000000000008800000000000000000000008800000088000000000000000000000088000000880000008800000000000000880000000000000088000
00000000000000000000000000000000000330000000000000000000000330000003300000033000000000000003300000033000000330000000000000033000
02222220088888300222222002222220022222200888883002222220022222200888883008888830088888300222222008888830022222200888883008888830
02022020080880300202202002022020020220200808803002022020020220200808803008088030080880300202202008088030020220200808803008088030
02222222088888320222222232222222022222220888883232222222322222220888883208888832388888323222222238888832022222223888883238888832
02222222088888320222222232222222022222220888883232222222322222220888883208888832388888323222222238888832022222223888883238888832
02022020080880300202202002022020020220200808803002022020020220200808803008088030080880300202202008088030020220200808803008088030
02222220088888300222222002222220022222200888883002222220022222200888883008888830088888300222222008888830022222200888883008888830
00000000000000000003300000000000000000000003300000033000000000000000000000033000000330000003300000000000000330000000000000033000
00000000000000000000000000000000000330000000000000000000000330000003300000033000000000000003300000033000000330000000000000033000
08888880022222300888888008888880088888800222223008888880088888800222223002222230022222300888888002222230088888800222223002222230
08088080020220300808808008088080080880800202203008088080080880800202203002022030020220300808808002022030080880800202203002022030
08888888022222380888888838888888088888880222223838888888388888880222223802222238322222383888888832222238088888883222223832222238
08888888022222380888888838888888088888880222223838888888388888880222223802222238322222383888888832222238088888883222223832222238
08088080020220300808808008088080080880800202203008088080080880800202203002022030020220300808808002022030080880800202203002022030
08888880022222300888888008888880088888800222223008888880088888800222223002222230022222300888888002222230088888800222223002222230
00000000000000000003300000000000000000000003300000033000000000000000000000033000000330000003300000000000000330000000000000033000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
66666600666666006666660066666600666666006666660066666600666666006666660066666600666666006666660066666600666666000000000000000000
66666660666666606666666066666660666666606666666066666660666666606666666066666660666666606666666066666660666666600000000000000000
66662662666686686662266266628668666826626668866866222662662286686628266266288668668226626682866866882662668886680000000000000000
66662662666686686662266266628668666826626668866866222662662286686628266266288668668226626682866866882662668886680000000000000000
66666660666666606666666066666660666666606666666066666660666666606666666066666660666666606666666066666660666666600000000000000000
66666600666666006666660066666600666666006666660066666600666666006666660066666600666666006666660066666600666666000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00088000000880000008800000088000000880000008800000088000000880000008800000088000000880000008800000088000000880000000000000000000
66688600666886006668860066688600666886006668860066688600666886006668860066688600666886006668860066688600666886000000000000000000
66666660666666606666666066666660666666606666666066666660666666606666666066666660666666606666666066666660666666600000000000000000
66662662666686686662266266628668666826626668866866222662662286686628266266288668668226626682866866882662668886680000000000000000
66662662666686686662266266628668666826626668866866222662662286686628266266288668668226626682866866882662668886680000000000000000
66666660666666606666666066666660666666606666666066666660666666606666666066666660666666606666666066666660666666600000000000000000
66688600666886006668860066688600666886006668860066688600666886006668860066688600666886006668860066688600666886000000000000000000
00088000000880000008800000088000000880000008800000088000000880000008800000088000000880000008800000088000000880000000000000000000
__map__
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000c2a402a31402a2e2e2e2e2e2e2e2a2e2e2e2e2e2e23000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000282e052b2d2d000000000000002d00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000284c272d000000000000002840402e2e260000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000002d0000000000000000000000002d0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000252e2a2a2f2a2a2a2a2600000000000000292a2a2a2a2a2a2600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000002d2560606160616061612a2e2a31412e2e616161616161616100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000002d282e2e2e2e2e2e2e2e2715041b1d0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000282e2a2a2a2a2a2a2a26001d283c170000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000001561616061606160601a170000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000181e1e1e1e1e1e1e1e17000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
__sfx__
01080020219001f9001c9001a90018900189000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
a90400041810000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
491000080000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
010800200c023189001c9001a900189001d9001a9001c9000c0001c9001c9001c9001c9001c9001f900219000c0231c9001a900189001a9001c9001c9001f9000c000219001f9001c9001a900189001890000000
010800200c000189001c9001a900189001d9001a9001c9000c0231c9001c9001c9001c9001c9001f900219000c0001c9001a900189001a9001c9001c9001f9000c023219001f9001c9001a900189001890000000
011000080c0230c6130c6130c613246150c6130c6130c613000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
61100008280402804028045280450c023000002804028045000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
791000101606016061160651606116051160411603116021160151d0001a000160601606116065160601606522000220002200022000220002200022000220002200000000000000000000000000000000000000
c1100010160101601116015160100c023160001a0101a0111a0151a0101a015160100c02316010160101601500000000000000000000000000000000000000000000000000000000000000000000000000000000
79100010160601606116065160610c023220002200022000220001d0001a000160600c02322000160601606500000000000000000000000000000000000000000000000000000000000000000000000000000000
9110200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e0000e5440e540
911000000e5300e5300e5300e5300e5300e5300a5300a5300a5300a5300a5300a5300a5300a5300c5300c5300c5300c5300c5300c5300c5300c53007530075300752007520075200752007520075200752007520
c11000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001a7441a740
91100000213202132021320213202132021320213202132021320213202132021320213202132021320213202132021320213202132021320213201f3201f3201f3201f3201f3201f3201f3201f3202132021320
911000000752007520075200752007520075200752007520075200752007520075200752007520075200752007520075200752007520075200752007520075200752007520075200752007520075200e5400e540
911000002132021320213202132021320213202132021320213202132021320213202132021320213202132021320213202132021320213202132021320213202132021320213202132021320213202132021320
__music__
00 080a074b
00 080b0c07
00 080a074b
00 080b0c07
00 48090a44
00 48090b0c
00 48090a44
00 0e090b0c
01 0f090a11
02 12090d13

