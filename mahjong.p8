pico-8 cartridge // http://www.pico-8.com
version 33
__lua__
-- drawing functions

function dbg(str)
	--placeholder to satisfy 
	--syntax check
end

function draw_tile(t,x,y,r,f,u)
	if (u == 1) then
		t = 10 --upside-down tile
	elseif (u == 2) then
		-- this is a held tile
		if (f) then
			if (r) then spr(13,x,y)
			else        spr(14,x,y) end
		else 
			if (r) then spr(11,x,y) 
			else        spr(12,x,y) end
		end
		return
	end
	
	-- sprite slots 107 and 108 
	-- are reserved for rotations
	if (r) then
		rot_spr(t,107)
		rot_spr(t+16,108)
		spr(107,x,y,2,1,f,not f)
	else
		spr(t,x,y,1,2,f,f)
	end
end

function draw_all_tiles()
	for i,v in ipairs(tiles) do
		if (v.redraw) then
			v.redraw = false
			if (v.shade) then
				pal(7,10)
			end
			local r = 
				(v.dir=="l") or (v.dir=="r")
			local f = 
				(v.dir=="d") or (v.dir=="r")
			--local x = ((i-1)%18)*7
			--local y = ((i-1)\18)*15
			if (v.shrink) then
				if (r) then
					clip(v.x+1,v.y,13,8)
					draw_tile(
						v.t,v.x-1,v.y,r,f,v.pos
					)
				else
				 clip(v.x,v.y+1,8,13)
					draw_tile(
						v.t,v.x,v.y-1,r,f,v.pos
					)
				end
			else
				draw_tile(
					v.t,v.x,v.y,r,f,v.pos
				)
			end
			clip(0,0,127,127)
			pal(7,7)
		end
	end
end

function draw_cursor(t)
	if (type(t) == "number") then
		t = tiles[t]
	end
	if (not t) then return end
	if (
		(t.dir=="l") or (t.dir=="r")
	) then
		spr(31,t.x-8,t.y)
	else
		spr(15,t.x,t.y-8)
	end
end

function _init()
	-- sets black to be opaque
	palt(0, false)
	-- sets beige to be transparent
	palt(15, true)
	
	setup()
end

ghcalls = 0

function _update()
	dbg("_update start: "..stat(1))
	ghcalls = 0 -- for perf dbg
	
	-- shade the selected tile
	dbg("sel = "..sel)
	for i,v in ipairs(ph) do
		local shade_me = (i == sel)
		if (
			tiles[ph[i]].shade ~= shade_me
		) then
			tiles[ph[i]].redraw = true 
			tiles[ph[i]].shade = shade_me
		end
	end
	
	-- unforgivable hack: mark the
	-- first element of the wall
	-- for redraw as a way to clear
	-- the previous wall count
	if (#wall > 0) then
		tiles[wall[1]].redraw = true
	end
	
	-- handle input
	if (not dirpush_d) then
		if (btn(⬅️) or btn(⬇️)) then
			sel = sel-1
			sel = bigger(sel,1)
		elseif (btn(➡️) or btn(⬆️)) then
			sel = sel+1
			sel = lesser(sel,#ph)
		end
		justmoved = true
	end
	
	-- dumb way to slow down
	-- moving selection on first
	-- press (to make it easier to
	-- move by one tile at a time)
	dirpush_d = dirpush()
	
	-- if discard button pressed
	if (btnp(🅾️) and #wall>0) then
		discard_ph()
		place_ph()
		draw_ph()
	end

	-- for testing: allow selected
	-- tile to be modified
	if (btnp(🅾️,1)) then
		local tile = tiles[ph[sel]]
		tile.redraw = true
		while true do
			tile.t = tile.t + 1
			if (tile.t > 80) then
				tile.t = 1
			end
			
			if (valid_sprites[tile.t]) then
				break
			end
		end
	end
	
	update_dbg()
	dbg("_update end: "..stat(1))
end

function _draw()
	--rectfill(0,0,127,127,2)

	dbg("_draw start: "..stat(1))

	dbg("before draw_tiles: "..stat(1))
	draw_all_tiles()
	dbg("after draw_tiles: "..stat(1))
	
	local str = tostr(#wall)
	cursor(81-2*#str,46)
	print(#wall, 8)
	
	-- test out our grouping 
	-- functions
	
	local hand_sorted = cpy(ph)
	sort_tiles(hand_sorted)
 
	local test_hand = {
		grps = {},
		ungrped = hand_sorted
	}
	
	local hands = {}
 dbg("before gh: "..stat(1))
	grphand(test_hand,hands)
 dbg("after gh: "..stat(1))

 local winner = false
	for _,h in ipairs(hands) do
		if (is_winning(h)) then
			winner = true
			break
		end
	end
	
	
	rectfill(16,16,120,32,2)
	color(8)
	cursor(16,24)
	if (winner) then
		print("this is a winning hand", 8)
	else
		print("this is not a winning hand")
	end
	
	-- test our waits function by
	-- asking what the waits are
	-- should the user discard the
	-- selected tile.
	
	local waits_test = {
		grps = {},
		ungrped = cpy(ph)
	}
	deli(waits_test.ungrped,sel)
	sort_hand(waits_test)
	dbg(waits_test.ungrped)
	
	local grpings = {}
	grphand(waits_test,grpings)
	
	sort_hands(grpings)
	
	for i,hand in ipairs(grpings) do
		-- this 5 is a wild guess,
		-- but there should be some 
		-- upper bound on maximum
		-- number of ungrouped tiles
		-- for a tenpai hand
		if (#(hand.ungrped) < 5) then
			local tmp = {
				ungrped = hand.ungrped
			}
			local ws = waits(tmp)
			if (#ws > 0) then
				dbg("waits")
				dbg(ws)
			end
		end
	end
	
	dbg("")
	dbg("calls: " .. ghcalls)
	dbg("--------------------")
	
	dbg("#hands = "..#hands)
	for i,hand in ipairs(hands) do
		if (#(hand.ungrped) == 0) then
			dbg("hand " .. i .. ":")
			for _,grp in ipairs(hand.grps) do
				dbg(grp)
			end
			--dbg("ungrouped:")
			--dbg(hand.ungrped)
		end
	end

	dbg("_draw end: "..stat(1))
	print_dbg()
end
-->8
-- helpers

function lesser(a,b)
	if (a<b) then
		return a
	else
		return b
	end
end

function bigger(a,b)
	if (a>b) then
		return a
	else
		return b
	end
end

-- rotates sprite t by 90 deg
-- ccw and saves to sprite y
function rot_spr(t,y)
	local tile = {}

	local tad=(t%16)*4+(t\16)*64*8
	local yad=(y%16)*4+(y\16)*64*8
	
	for i = 0,7 do
		for j = 0,3 do
			local off = i*64+j
			local byte = peek(tad+off)
			local lcol = band(byte, 0xf)
			local rcol = byte\16
			tile[#tile+1] = lcol
			tile[#tile+1] = rcol
		end
	end
	
	for i = 0,7 do
		for j = 0,3 do
			local off = i*64+j
			local lo = tile[j*2*8+i+1]
			local hi = tile[j*2*8+8+i+1]
			local byte = shl(hi,4) + lo
			poke(yad+off,byte)
		end
	end
end

dirpush_d = false

function dirpush()
	return band(btnp(),0xf) ~= 0
end

function shuf(arr)
	local len = #arr
	while (len > 1) do
		local ind = flr(rnd(len))+1
		local tmp = arr[ind]
		arr[ind] = arr[len]
		arr[len] = tmp
		len = len - 1
	end
end

-- returns index of element in
-- arr, or nil if not found
function afind(arr, el)
	for i,v in ipairs(arr) do
		if arr[i] == el then
			return i
		end
	end
	
	return nil
end

-- table shallow copy
function cpy(t) 
	local ret = {}
	for k,v in pairs(t) do
		ret[k] = v
	end
	
	return ret
end

function dtab(t)
	if (type(t) ~= "table") then
		print("not a table")
	end
	
	for k,v in pairs(t) do
		print(tostr(k).."="..v)
	end
end

-- turns out it actually is 
-- worth it to implement quick
-- sort. i'll get to that at
-- some point...
function sort(arr,cmp)
	cmp = cmp or function(a,b)
		return a < b
	end
	
	for i = 1,#arr do
		for j = 1,#arr-1 do
			if (
				cmp(arr[j+1],arr[j])
			) then
				local tmp = arr[j]
				arr[j] = arr[j+1]
				arr[j+1] = tmp
			end
		end
	end
end

function tprint (tbl)
  for k, v in pairs(tbl) do
    if type(v) == "table" then
        tprint(v)
    else
        print(v,7)
    end
  end
end

function pk(a)
	print(peek(a))
end
-->8
-- tiles

tnames = {
	"green dragon",
	"red dragon",
	"white dragon",
	"east","south","west","north"
}

-- given tile name, return the
-- sprite number for top half
tmap = {}

for i = 1,9 do
 tnames[#tnames+1] = "man"..i
 tnames[#tnames+1] = "pin"..i
 tnames[#tnames+1] = "sou"..i
	tmap["man"..i] = i
	tmap["pin"..i] = 32+i
	tmap["sou"..i] = 64+i
end

tmap["green dragon"] = 74
tmap["red dragon"]   = 75
tmap["white dragon"] = 76

tmap["east"]  = 42
tmap["south"] = 43
tmap["west"]  = 44
tmap["north"] = 45


-- each tile has:
-- tile coordinate
-- x pos, y pos
-- facing n/s/e/w
-- face-down/face-up/held

face_up = 0
face_down = 1
held = 2
function mktile(n)
	return {
		t = tmap[n],
		x = -20, y = -20,
		dir = "d",
		pos = face_up
	}
end

tiles = {}
for i = 1,4 do
	for j = 1,9 do
		tiles[#tiles+1] =
			mktile("man"..j)
		tiles[#tiles+1] =
			mktile("pin"..j)
		tiles[#tiles+1] =
			mktile("sou"..j)
	end
	tiles[#tiles+1]=mktile "east"
	tiles[#tiles+1]=mktile "south"
	tiles[#tiles+1]=mktile "west"
	tiles[#tiles+1]=mktile "north"
	tiles[#tiles+1] =
		mktile "green dragon"
	tiles[#tiles+1] =
		mktile "red dragon"
	tiles[#tiles+1] =
		mktile "white dragon"
end

-- sorts tiles based on sprite
-- coordinate. could have been
-- cleaner but whatever
function tile_lt(t1,t2,convert)
	local tc1 = t1
	local tc2 = t2
	if (convert) then
		tc1 = tiles[t1].t
		tc2 = tiles[t2].t
	end
	if (tc1%16>9) then
		if (tc2%16>9) then
			return tc1 < tc2
		else
			return false
		end
	else
		if (tc2%16>9) then
			return true
		else
			return tc1 < tc2
		end
	end
end

-- takes an array of tile 
-- indices and sorts them
-- as man,pin,sou,wind,dragons
-- and in numerical order within
-- suits
function sort_tiles(arr,cvt)
	if (cvt == nil) then
		cvt = true
	end
	
	--whatever man, just use
	--bubble sort
	for i = 1,#arr do
		for j = 1,#arr-1 do
			if (
				tile_lt(arr[j+1],arr[j],cvt)
			) then
				local tmp = arr[j]
				arr[j] = arr[j+1]
				arr[j+1] = tmp
			end
		end
	end
end



-- only used for debugging
valid_sprites = {}

for k,v in pairs(tmap) do
	valid_sprites[v] = true
end
-->8
--animations and game state

-- symbolic names to make code
-- nicer to read
pdiscard = 0
rdiscard = 1
udiscard = 2
ldiscard = 3


-- i make my life easier by
-- assuming: only one tile 
-- is moving at a time

-- when this is 0, no anims are
-- active and we can take user
-- input
frames_left = 0



-->8
-- game logic

-- hands
-- todo: deal with called sets
ph = {} -- player
lh = {} -- left
uh = {} -- upper
rh = {} -- right
-- discards, a.k.a. ponds
pd = {}
ld = {}
ud = {}
rd = {}

-- other piles
wall = {}
kan_draws = {53,54,55,56}
doras = {57,58,59,60}
uradoras = {60,62,63,64}

-- other tokens
riichi_sticks = {}
dealer = {}
round_wind = {}


--there's probably a nicer way
--to do this...
function place_ph()
	sort_tiles(ph)
	for i = 1,#ph do
		local t = tiles[ph[i]]
		t.y = 111
		t.x = 8*i+4
		t.pos = face_up
		t.dir = "u"
		t.redraw = true
	end
end

function place_lh()
	sort_tiles(lh)
	for i = 1,#lh do
		local t = tiles[lh[i]]
		t.y = 8*i+4
		t.x = 0
		t.pos = held
		t.dir = "l"
		t.redraw = true
	end
end

function place_uh()
	sort_tiles(uh)
	for i = 1,#uh do
		local t = tiles[uh[i]]
		t.y = 0
		t.x = 116 - 8*i
		t.pos = held
		t.dir = "u"
		t.redraw = true
	end
end

function place_rh()
	sort_tiles(rh)
	for i = 1,#rh do
		local t = tiles[rh[i]]
		t.x = 120
		t.y = 116 - 8*i
		t.pos = held
		t.dir = "r"
		t.redraw = true
	end
end

--todo: add animation
function draw_ph()
	local drawn = deli(wall,1)
	ph[#ph+1] = drawn
	sel = #ph
	local t = tiles[drawn]
	t.x = 95
	t.y = 104
	t.pos = face_up
	t.dir = "l"
	t.redraw = true
end

function discard_ph()
	local shade = (sel == #ph)
	local tnum = deli(ph,sel)
	pd[#pd+1] = tnum
	local num = #pd - 1
	local pond_x = num%6
	local pond_y = num\6
	if (pond_y>2) then
		pond_y = 2
		pond_x = num - 12
	end
	local t = tiles[tnum]
	t.x = 40 + pond_x*8
	t.y = 68 + pond_y*14
	t.pos = face_up
	t.dir = "u"
	t.shrink = true
	t.shade = shade
	t.redraw = true
end

function force_redraw()
	rectfill(0,0,127,127,2)
	
	for i = 1,#tiles do
		tiles[i].redraw = true
	end
end

function setup()
	shuf(tiles)
	
	force_redraw()
	
	for i = 1,#tiles do
		tiles[i].shirnk = false
		tiles[i].shade = false
		tiles[i].redraw = true
	end
	
	-- draw 13 tiles to the 
	-- players' hands
	
	for i = 1,13 do
		ph[i] = i
		lh[i] = i+13
		uh[i] = i+26
		rh[i] = i+39
	end
	
	-- kan tiles
	for i = 0,3 do
		local t = tiles[i+53]
		t.x = 40+(i\2)*8
		t.y = 52
		t.dir = "u"
		t.pos = face_down
	end
	
	-- doras
	for i = 0,3 do
		local t = tiles[57+i]
		t.x = 56+i*8
		t.y = 52
		t.dir = "u"
		t.pos = face_down
		--hide uradoras
		tiles[61+i].x = -100
	end
	-- turn over first dora
	tiles[57].pos = face_up
	
	-- wall tiles
	wall = {}
	for i = 65,136 do 
		wall[#wall+1] = i
		tiles[i].x = 73
		tiles[i].y = 44
		tiles[i].pos = face_down
		tiles[i].dir = "r"
	end
	
	place_ph()
	place_lh()
	place_uh()
	place_rh()
	
	-- draw dealer's first tile
	-- for now we pretend player
	-- is always the dealer
	draw_ph()
	
	sel = 14
end


-->8
-- help text

controls = [[
arrows to move cursor. x to
toggle menu. z to discard the
selected tile or activate the
selected menu item.
]]

tut = [[
on your turn, you draw a tile
(it'll be shown above your hand)
then discard one. your goal is
to replace your tiles one-by-one
to make patterns. you win when
your hand plus your newly drawn
tile form four groups of three
plus one pair. a group can be a
run or a triple, and must be the
same suit (wind and dragon tiles
don't form runs). there are four
extra rules and a scoring scheme 
see "kans", "calling", "riichi",
"yakus", "scoring", and "doras".
you may also be interested in 
"special cases".
]]

kans = [[
in some cases, it is possible to
have four-of-a-kind. this is 
called a "kan". if you wish to
treat four (identical) tiles as 
one group, you must declare them
as a kan. (the alternative would 
be to use the tiles in more than 
one pattern. e.g. three of them 
could be a triple while the 
fourth is part of a run). when 
you declare a kan, it is placed 
on your right and can no longer 
be changed. you will also draw 
an extra tile so you can still
make four pattern groups and one 
pair (i.e. instead of drawing 
one and discarding one, you end
up drawing two, making a kan, 
then discarding one).
]]

calling = [[
when an opponent discards a tile
you may immediately take it if 
it can form a triple or a run. 
to prove that this is the case, 
this newly made set is placed 
face-up on your right, and can 
no longer be changed. taking a 
discard to form a run is called
"chi", taking a discard to form
a triple is called "pon", and
taking a discard to form a kan
is called "kan". also, if the
stolen tile completes your whole
hand it is called "ron". "chi" 
may only be called on the player 
to your left, but the others can 
be called on  anyone. after 
calling, play proceeds from the 
player to your right.
]]

riichi=[[
if you have not called any sets,
your hand is "closed". (note:
declaring a kan from four self-
drawn tiles still maintains this
"closed" status). if your hand
needs only the drawn tile to be
complete, it is said to be "in
tenpai". when you have a closed
hand in tenpai, you may call 
"riichi". when you do this, you
place a 1000-point stick in the
center of the table amd the 
winner of the hand receives it.
when in riichi, you can call ron
or draw a tile to complete your 
hand, but you may not discard 
any of the tiles youhad when you
originally called riichi. to
understand why you would want to
do this, please see "yakus".
]]

yakus=[[
in order to win a hand, you need
more than just four groups and a 
pair; you must also meet at
least one "yaku" condition. 
these "yakus" are mostly
arbitrary and there is just a 
big list you need to know about.
you can find such a list at
riichi.wiki/list_of_yaku.
by the way, winning a hand with
riichi counts as a yaku.
]]

scoring = [[
the value of hand is measured
in two quantities: "fu" (called
"minipoints") and "han" (called
"big points"). you earn 20 fu
for winning a hand. you also
earn fu for triples and kans.
closed groups are worth more, 
and groups of wind/dragon tiles
are worth even more. you earn
han from your yakus, and from
"doras" (see the "doras" 
tutorial). your final score is
something like fu times 2 to the
power of han, but there are some
extra rules about rounding. 
there is a great description in
chapter 13 of "barticle's 
japanese mahjong guide".
]]

doras = [[
dora tiles are shown in the very
center of the board. at the 
start of the game, there is only
one dora shown as the third face
up tile in that big face-down
pile. when a kan is called, the
next dora tile is turned face
up. the "successor" of one of
these face-up tiles is worth one
han. for example, if the four of
bamboo is face-up, that means
each five of bamboo in your hand
is worth one han. if a nine is
face-up, the one of the same 
suit is worth one han. the order
for dragons is g->r->w, and for
the wind tiles it is e->s->w->n.
]]

special_cases = [[
if you win a hand with riichi,
the files underneath the face-up
dora tiles are also turned face-
up, giving you twice as many 
chances to have doras in your 
hand. if you have called riichi,
and _any_ of your discarded 
tiles is one that could complete
your hand, you are not allowed 
to call ron, but you can still 
win with a self-drawn tile. this
is called being "in furiten".
]]
-->8
-- scoring

-- a hand is a table with two 
-- elements:
-- - grped, an array of groups,
--   where each group is an 
--   array of tile ids
-- - ungrped, a plain array of
--   ungrped tiles
-- the mkgrps function then just
-- returns a list of possible
-- hands


-- takes a hand and says whether
-- it is a winning hand. todo:
-- report on yakus
function is_winning(hand)
	if (#(hand.ungrped) > 0) then
		return
	end
		
	local numpairs = 0
	for _,v in ipairs(hand.grps) do
		if #v == 2 then
			numpairs = numpairs + 1
		end
	end
	
	if (
		numpairs==7 or numpairs==1
	) then
		return true
	else
		return false
	end
end

-- takes ungrouped tiles and 
-- returns an array of possible
-- tile ids (aka sprite numbers)
-- that could complete a tile
-- grouping. returns an empty
-- list if impossible.
function waits(hnd,numpairs,cvt)
	-- uhh..... what's the cleanest
	-- way to do this?
	
	-- idea 1, naive method: just
	-- try every single possible
	-- tile and see if it can
	-- complete the ungrouped sets
	
	-- well, idea 1 is pretty easy,
	-- so lets just try it
	
	-- performance really suffering
	
	local ret = {}
	
	local ung = hnd.ungrped
	local tarr = {}
	if (cvt == nil) then
		cvt = true
	end
	if (cvt) then
		for i = 1,#hnd.ungrped do
			add(tarr,tiles[ung[i]].t)
		end
	else
		for i = 1,#hnd.ungrped do
			add(tarr,ung[i])
		end
	end
	
	-- try and cut down on needless
	-- calls to grphand
	local totry = {}
	for i,t in ipairs(tarr) do
		totry[t] = true
		if (t%16<9) then
			totry[t+1] = true
			if (t%16>1) then
				totry[t-1] = true
			end
		end
	end
	
	local h = hnd
	
	for k,t in pairs(tmap) do
		if (totry[t]) then
			h.ungrped = cpy(tarr)
			add(h.ungrped,t)
			sort_tiles(h.ungrped, false)
			local hands = {}
			ghcalls = ghcalls + 1
			grphand(h,hands,false)
			for _,hand in ipairs(hands) do
				if (#(hand.ungrped) == 0) then
					add(ret,t)
					break
				end
			end
		end
	end
	
	return ret
end

-- little helper to deep copy
-- a hand
function hndcpy(h)
	local ret = {}
	ret.grps = cpy(h.grps)
	ret.ungrped = cpy(h.ungrped)
	return ret
end

function sort_hand(hnd)
	sort_tiles(hnd.ungrped)
end

-- helper to sort hands by 
-- ascending number of
-- ungrouped tiles
function sort_hands(hands)
	sort(hands, function(h1,h2)
		return 
			#(h1.ungrped) < #(h2.ungrped)
		end
	)
end

-- enumerates all possible hands
-- in arr and returns them in 
-- lst.
-- assumes ungrouped tiles are
-- sorted.
-- call this function using:
--
--		sort_hand(hand)
--  local my_lst = {}
--  grphand(hand,my_lst)
--
-- by the way, hnd is never
-- edited by this function
function grphand(hnd,lst,cvt,b)
 -- b is the index of where we
 -- should start looking through
 -- ungrouped tiles. this line
 -- makes it so callers don't
 -- have to pass it as an arg
	b = b or 1
	
	if (cvt == nil) then
		cvt = true
	end
	
	local ung = hnd.ungrped
	local tarr = {}
	if (cvt) then
		for i = b,#hnd.ungrped do
			add(tarr,tiles[ung[i]].t)
		end
	else
		for i = b,#hnd.ungrped do
			add(tarr,ung[i])
		end
	end
	
	---------------
	-- base case --
	---------------
	if (#tarr < 2) then
		--dbg("base case")
		add(lst,hnd)
		return
	end
	
	--------------------
	-- recursive step --
	--------------------
	
	-- try forming a pair
	if (tarr[1] == tarr[2]) then
		local nxt = hndcpy(hnd)
		-- remove first two ungrped
		deli(nxt.ungrped,b)
		deli(nxt.ungrped,b)
		-- add them as a new group
		add(
			nxt.grps,
			{ung[b], ung[b+1]}
		)
		--dbg("trying a pair")
		grphand(nxt,lst,cvt,b)
	end
	
	-- try forming a triple
	if (
		tarr[1] == tarr[2] and
		tarr[2] == tarr[3]
	) then
		local nxt = hndcpy(hnd)
		-- remove first three ungrped
		deli(nxt.ungrped,b)
		deli(nxt.ungrped,b)
		deli(nxt.ungrped,b)
		-- add them as a new group
		add(
			nxt.grps,
			{ung[b],ung[b+1],ung[b+2]}
		)
		--dbg("trying a triple")
		grphand(nxt,lst,cvt,b)
	end
	
	-- try forming four-of-a-kind
	-- note: this will eventually
	-- be removed, because kans
	-- must always be called and
	-- thus will already be grouped
	if (
		tarr[1] == tarr[2] and
		tarr[2] == tarr[3] and
		tarr[3] == tarr[4]
	) then
		local nxt = hndcpy(hnd)
		-- remove first four ungrped
		deli(nxt.ungrped,b)
		deli(nxt.ungrped,b)
		deli(nxt.ungrped,b)
		deli(nxt.ungrped,b)
		-- add them as a new group
		add(
			nxt.grps,
			{
				ung[b], ung[b+1],
				ung[b+2], ung[b+3]
			}
		)
		--dbg("trying a kan")
		grphand(nxt,lst,cvt,b)
	end
	
	-- try forming a run
	local run_found = true -- iupg
	local t1 = tarr[1]
	if (t1%16 > 7) then
		-- a run can't start above 7
		run_found = false
	end
	local i2 = afind(tarr,t1+1)
	local i3 = afind(tarr,t1+2)
	if (i2==nil or i3==nil) then
		run_found = false
	end
	
	if (run_found) then
		if (i2 > i3) then
			local tmp = i2
			i2 = i3
			i3 = tmp
		end
		local nxt = hndcpy(hnd)
		-- remove run from ungrped
		deli(nxt.ungrped,b+i3-1)
		deli(nxt.ungrped,b+i2-1)
		deli(nxt.ungrped,b)
		-- add run to groups
		add(
			nxt.grps,
			{
				ung[b],
				ung[b+i2-1],
				ung[b+i3-1]
			}
	 )
		--dbg("trying a run")
		grphand(nxt,lst,cvt,b)
	end
	
	-- try all groupings past this
	-- element
	--dbg("skipping this element")
	grphand(hnd,lst,cvt,b+1)
	
	return
end
-->8
--debug

dbg_str = {"debug mode"}
dbg_off = 0
dbg_en = false

function arr2str(arr)
	local delim = ""
	local str = "{"
	for _,v in ipairs(arr) do
		str = str..delim..tostr(v)
		delim = ","
	end
	return str.."}"
end

function dbg(x)
	if (dbg_en) then
		if (type(x) == "table") then
			add(dbg_str,arr2str(x))
		else
			add(dbg_str,x)
		end
	end
end

-- place this at the bottom of
-- the _update function
function update_dbg()
	-- player 1 controls toggle the
	-- debug mode
	if (btnp(❎,1)) then
		dbg_en = not dbg_en
		dbg_off = 0
		force_redraw()
	end
	
	if (dbg_en) then
		if (btn(⬇️,1)) then
			dbg_off = dbg_off+1
		elseif (btn(⬆️,1)) then
			dbg_off = 
				bigger(dbg_off-1,1)
		end
	end
end

-- place this at the bottom of
-- the _draw function
function print_dbg()	
	if (dbg_en) then
		dbg_off = 
			lesser(dbg_off,#dbg_str)
		-- there must be something
		-- more elegant than this...
		local l = bigger(dbg_off,1)
		      l = lesser(l,#dbg_str)
		local r = 
			lesser(l+19,#dbg_str)
		
		cursor(0,0)
		cls(1)
		color(6)
		
		for i = l,r do
			print(dbg_str[i])
		end
		
		-- lua has good garbage 
		-- collection... right?
		dbg_str = {"debug mode"}
	end
end


__gfx__
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffbbbff
fffffffff7777777f7777777f7700077f7777777f7070000f7770777f7707777f7707777f7077007f9999999ff77779ffffffffff97777fff9999999fffbbbff
fffffffff7777777f7700077f7077777f0000000f7077077f7770700f7707700f7700777f7000777f9999999ff77779ff7777777f97777fff7777777fffbbbff
fffffffff7770007f7777777f7770077f0707070f7010017f0000007f7707077f7770077f7070777f9999999ff77779ff7777777f97777fff7777777fffbbbff
fffffffff7000777f7770007f7007777f0707070f0070717f7777777f7700777f7707007f0770777f9999999ff77779ff7777777f97777fff7777777fbbbbbbb
fffffffff7777777f7000777f7777007f7000007f0000717f7707007f7707777f7007700f0770707f9999999ff77779ff7777777f97777fff7777777ffbbbbbf
fffffffff7777777f7777777f7000077f7777777f7077000f0007707f7070007f0077777f7777000f9999999ff77779ff9999999f97777fffffffffffffbbbff
fffffffff7777777f7777777f7777777f7777777f7000070f7777777f7777777f7777777f7777777f9999999ff77779ffffffffff97777ffffffffffffffbfff
00000000f8888888f8888888f8888888f8888888f8888888f8888888f8888888f8888888f8888888f9999999ffffffffffffffffffffffffffffffffffffbfff
00000000f7778777f7778777f7778777f7778777f7778777f7778777f7778777f7778777f7778777f9999999ff6777777777777777777777777776ffffffbbff
00000000f7888887f7888887f7888887f7888887f7888887f7888887f7888887f7888887f7888887f9999999f677777777777778877777777777776fbbbbbbbf
00000000f7878787f7878787f7878787f7878787f7878787f7878787f7878787f7878787f7878787f9999999f777777777777788887777777777777fbbbbbbbb
00000000f7888887f7888887f7888887f7888887f7888887f7888887f7888887f7888887f7888887f9999999f777777777777788887777777777777fbbbbbbbf
00000000f7878787f7878787f7878787f7878787f7878787f7878787f7878787f7878787f7878787f9999999f677777777777778877777777777776fffffbbff
00000000f8888888f8888888f8888888f8888888f8888888f8888888f8888888f8888888f8888888f9999999ff6777777777777777777777777776ffffffbfff
00000000f8778778f8778778f8778778f8778778f8778778f8778778f8778778f8778778f8778778f9999999ffffffffffffffffffffffffffffffffffffffff
00000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff00000000
00000000f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777ff6776ff00000000
00000000f7777777f7777777f7111777f7777777f7777777f1117111f1117777f7777777f1111111f7770777f7770777f7777777f7777777f677776f00000000
00000000f7111117f7711177f7131777f1117111f1117111f1317131f1311177f1117111f1313131f7700077f0000000f0000000f7707077f777777f00000000
00000000f1133311f7133317f7111777f1317131f1317131f1117111f1113111f1317131f1111111f7770777f7770777f7707077f7707077f777777f00000000
00000000f1388831f7137317f7777777f1117111f1117111f7777777f7711131f1117111f7777777f0000000f0000000f7707077f7707077f777777f00000000
00000000f3878783f7133317f7777777f7777777f7777777f7777777f7777111f1117111f7777777f7070707f0777770f1101011f0007077f777777f00000000
00000000f3887883f7711177f7711177f7777777f7711177f7777777f7777777f1317131f1111111f7000007f0707070f1707071f7707070f777777f00000000
00000000f3878783f7777777f7718177f7777777f7718177f1117111f1117111f1117111f1818181f7070707f0770770f1707071f7707007f777777f00000000
00000000f3887883f7711177f7711177f7777777f7711177f1817181f1817181f1117111f1111111f7000077f0700070f1707001f7707077f777777f00000000
00000000f1388831f7133317f7777777f7777777f7777777f1117111f1117111f1317131f7777777f7700077f0770770f1077771f7007077f777777f00000000
00000000f1133311f7137317f7777777f1117111f1117111f7777777f7777777f1117111f7777777f7070707f0700070f1777771f0707077f777777f00000000
00000000f7111117f7133317f7771117f1317131f1317131f1117111f1117111f1117111f1111111f0770770f0770770f1111111f7707070f777777f00000000
00000000f7777777f7711177f7771317f1117111f1117111f1817181f1817181f1317131f1313131f7770777f0777707f7777777f7707000f777777f00000000
00000000f7777777f7777777f7771117f7777777f7777777f1117111f1117111f1117111f1111111f7070777f7777777f7777777f7777777f778877f00000000
00000000f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7700777f7777777f7777777f7777777f788887f00000000
00000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff00000000f788887f00000000
00000000f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f7777777f777777700000000f778877f00000000
00000000f7733377f7773777f7773777f7377737f7377737f7777777f7778777f7777777f7378737f7737737f7778777f777777700000000f777777f00000000
00000000f73b3b37f7773777f7773777f7377737f7377737f7373737f7778777f7377737f7378737f3733377f7778777f777777700000000f777777f00000000
00000000f3b3b3b3f7773777f7773777f7377737f7377737f7373737f7778777f7373737f7378737f7337337f7778777f777777700000000f777777f00000000
00000000fb3b3b3bf7773777f7773777f7377737f7377737f7373737f7778777f7337337f7378737f3377733f8888888f777777700000000f777777f00000000
00000000f3b311b3f7773777f7773777f7377737f7378737f7373737f7777777f7377737f7777777f7777777f8778778f777777700000000f777777f00000000
00000000fb88713bf7777777f7777777f7777777f7778777f7373737f7373737f7377737f7378737f3337333f8778778f777777700000000f777777f00000000
00000000f3b171b3f7777777f7777777f7777777f7778777f7777777f7373737f7777777f7378737f7737373f7888887f777777700000000f777777f00000000
00000000fb31713bf7777777f7777777f7777777f7778777f7777777f7373737f7377737f7378737f3337373f7778777f777777700000000f777777f00000000
00000000f3b171b3f7773777f7377737f7377737f7378737f7373737f7373737f7377737f7378737f3777777f7778777f777777700000000f777777f00000000
00000000fb17771bf7773777f7377737f7377737f7377737f7373737f7777777f7337337f7777777f3337333f7778777f777777700000000f777777f00000000
00000000f3177713f7773777f7377737f7377737f7377737f7373737f7373737f7373737f7378737f3737373f7778777f777777700000000f777777f00000000
00000000f7311137f7773777f7377737f7377737f7377737f7373737f7373737f7377737f7378737f7737737f7778777f777777700000000f677776f00000000
00000000f7787877f7773777f7377737f7377737f7377737f7373737f7373737f7777777f7378737f7337373f7778777f777777700000000ff6776ff00000000
00000000f7887887f7777777f7777777f7777777f7777777f7777777f7373737f7777777f7378737f7777777f7777777f777777700000000ffffffff00000000
00000000cccccccccccccccc888888888888888833333333333333339999999999999999ccccccccccccccccaaaaaaaaaaaaaaaa000000000000000000000000
00000000c77777777777777c877777777777777837777777777777739777777777777779cccc7ccccccccccca888a888a8a8a88a000000000000000000000000
00000000c777777c7777777c877777787777777837333333333333739777779797777779cccc7cccc777777ca8a8a8aaa8a8a8a8000000000000000000000000
00000000c77ccccccccc777c877888888888777837777737737777739777779797777779c7777777c7cccc7ca8a8a888a8a8a8a8000000000000000000000000
00000000c777777c7777777c877777787777777837777737737777739777779797779779cc7ccc7cc777777ca888aaa8a8a8a8a8000000000000000000000000
00000000c777ccccccc7777c877888888888777837333333333333739779999797797779cc7ccc7cc7cccc7ca88aaaa8a8a8a8a8000000000000000000000000
00000000c777c77c77c7777c877877777778777837377737737773739777779797977779cc7ccc7cc777777ca8a8a888aa8aa88a000000000000000000000000
00000000c777ccccccc7777c877878777878777837377737737773739777779799777779c7777777c7cccc7caaaaaaaaaaaaaaaa000000000000000000000000
00000000c777c77c77c7777c877877878778777837377737737773739777779797777779cccc7cccc777777c0000000000000000000000000000000000000000
00000000c777ccccccc7777c877877787778777837377377737373739777779797777779c7777777c7cccc7c0000000000000000000000000000000000000000
00000000c777777c7777777c877878888878777837373777733773739777999797777779cccc7cccc777777c0000000000000000000000000000000000000000
00000000c7777ccccc77777c877877787778777837377777777773739799779797777779ccc777cccc7c7ccc0000000000000000000000000000000000000000
00000000c777cc7c7cc7777c877878888878777837377777777773739777779797777979cc7c7c7ccc7c7ccc0000000000000000000000000000000000000000
00000000c77cc77c77cc777c877877787788777837333333333333739777779799999779c7cc7cc7cc7c7c7c0000000000000000000000000000000000000000
00000000c77777777777777c877777777777777837777777777777739777777777777779cccc7cccc7cc77cc0000000000000000000000000000000000000000
00000000cccccccccccccccc888888888888888833333333333333339999999999999999cccccccccccccccc0000000000000000000000000000000000000000
__sfx__
4920000c0f1700f1700f1700f1700f1700f17011170111700c1700c1700f1700f1702210024100241002710027100271002910029100291002b1002b1002b1002e1002e1001b1001d100271002b1001b1001d100
91200c001b3701f370223701f3701b3702b3001d3702e300183702e30022370223702230024300243002730000300003000030000300003000030000300003000030000300003000030000300003000030000300
0d200c001b5001f500225001f5001b5002b5702b5752e5702e5752e5702e5752e5002250024500245002750000500005000050000500005000050000500005000050000500005000050000500005000050000500
0120000c1b0701f070220701f0701b0702b0701d0702e070180702e07022070220702200024000240002700000000000000000000000000000000000000000000000000000000000000000000000000000000000
__music__
03 40010243

