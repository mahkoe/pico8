-- take the given 1-d array of
-- tiles and split it into an
-- array of patterns. it is 
-- very possible that only one
-- patter will be returned.
-- returns nil if there is no
-- valid way to split.
-- to be a little more flexible,
-- if the part input is given 
-- (and is not false) then this
-- function returns as many
-- valid groups as possible.
-- the array must be sorted 
-- first.
function split_grps(arr, part)
	local tarr = {}
	for i,v in ipairs(arr) do
		tarr[i] = tiles[arr[i]].t
	end

	----------------
	-- base cases --
	----------------
	if (#arr < 2) then
		dbg("too few for a group")
		return nil 
	elseif (#arr == 2) then
		-- two elements can only be
		-- a pair
		if (tarr[1] == tarr[2]) then
			dbg("base case/pair")
			return {arr}
		else 
			dbg("unmatching pair")
			return nil 
		end
	elseif (#arr == 3) then
		-- three elements can be a 
		-- run in man/pin/sou or a
		-- triple
		if (
			tarr[1] == tarr[2] and
			tarr[2] == tarr[3]
		) then
			dbg("base case triple")
			return {arr}
		else
			local t1 = tarr[1]
			local t2 = tarr[2]
			local t3 = tarr[3]
			if (t1 % 16 > 7) then
				-- honor tiles cannot form
				-- runs (and runs cannot
				-- start above 7)
				dbg("too high for run")
				return nil
			elseif (
				t2 - t1 ~= 1 or
				t3 - t2 ~= 1
			) then
				-- not a valid run
				dbg("not sequential")
				return nil
			else
				-- this is a valid run
				dbg("base case/run")
				return {arr}
			end
		end
	elseif (#arr == 4) then
		-- four elements can be a kan
		-- or two pairs
		if (
			(tarr[1] == tarr[2]) and
			(tarr[2] == tarr[3]) and
			(tarr[3] == tarr[4])
		) then 
			dbg("base case/kan")
			return {arr}
		elseif (
			(tarr[1] == tarr[2]) and
			(tarr[3] == tarr[4])
		) then
			dbg("base case/two pair")
			return {
				{arr[1],arr[2]},
				{arr[3],arr[4]}
			}
		else
			dbg("not four-of-a-kind")
			return nil
		end
	end
	
	-- use classic recursive 
	-- implementation for 
	-- backtracking. try to form a
	-- group with the first tile in
	-- the array, then recurse to 
	-- see if the rest of the array
	-- can form groups
	
	-- try forming a pair
	if (tarr[1] == tarr[2]) then
		local rest = {}
		for i = 3,#arr do
			rest[i-2] = arr[i]
		end
		dbg("trying a pair")
		local grps = split_grps(rest);
		if (grps ~= nil) then
			grps[#grps+1] = {
				arr[1],arr[2]
			}
			return grps
		end
		dbg("pair failed")
	end
	
	-- try forming a triple
	if (
		tarr[1] == tarr[2] and
		tarr[2] == tarr[3]
	) then
		local rest = {}
		for i = 4,#arr do
			rest[i-3] = arr[i]
		end
		dbg("trying a triple")
		local grps = split_grps(rest);
		if (grps ~= nil) then
			grps[#grps+1] = {
				arr[1],arr[2],arr[3]
			}
			return grps
		end
		dbg("triple failed")
	end
	
	-- try forming four-of-a-kind
	-- TODO
	
	-- try forming a run
	local t1 = tarr[1]
	dbg("t1 = " .. t1)
	if (t1%16 > 7) then
		-- a run can't start above 7
		dbg("run failed (too big)")
		return nil
	end
	local i2 = afind(tarr,t1+1)
	local i3 = afind(tarr,t1+2)
	if (i2==nil or i3==nil) then
		--couldn't find whole run
		dbg("run failed (not all there)")
		return nil
	end
	
	dbg("trying a run")
	local rest = {}
	for i = 2,#arr do
		if (i~=i2 and i~=i3) then
			rest[#rest+1] = arr[i]
		end
	end
	
	local grps = split_grps(rest)
	if (grps == nil) then
		-- no valid grouping of rest
		-- of tiles
		dbg("nothing worked, returning")
		return nil
	end
	
	grps[#grps+1] = {
		arr[1], arr[i2], arr[i3]
	}
	
	return grps
end













-- enumerates all possible hands
-- in arr and returns them in 
-- lst
-- call this function with:
--
--  local my_lst = {}
--  mkgrps(hand,{},my_lst)
--
-- by the way, arr and cur are
-- not edited by this function
function mkgrps(arr,cur,lst)
	local tarr = {}
	for i,v in ipairs(arr) do
		tarr[i] = tiles[arr[i]].t
	end
	--dbg(tarr)

	-- note: this function only
	-- ever edits cur and lst, so
	-- it is safe to use arr by
	-- reference
	
	----------------
	-- base cases --
	----------------
	if (#arr < 2) then
		-- too few elements for group
		---dbg("base case/too few")
		return
	elseif (#arr == 2) then
		-- two elements can only be
		-- a pair
		if (tarr[1] == tarr[2]) then
			local c = cpy(cur)
			add(c,arr)
			add(lst,c)
			---dbg("base case/pair")
		end
		return
	elseif (#arr == 3) then
		-- three elements can be a 
		-- run in man/pin/sou or a
		-- triple
		if (
			tarr[1] == tarr[2] and
			tarr[2] == tarr[3]
		) then
			local c = cpy(cur)
			add(c,arr)
			add(lst,c)
			---dbg("base case/triple")
		else
			local t1 = tarr[1]
			local t2 = tarr[2]
			local t3 = tarr[3]
			if (t1 % 16 > 7) then
				-- honor tiles cannot form
				-- runs (and runs cannot
				-- start above 7)
				return
			elseif (
				t2 - t1 ~= 1 or
				t3 - t2 ~= 1
			) then
				return
			else
				local c = cpy(cur)
				add(c,arr)
				add(lst,c)
				--dbg("base case/run")
				return
			end
		end
	elseif (#arr == 4) then
		-- four elements can be a kan
		-- or two pairs
		if (
			(tarr[1] == tarr[2]) and
			(tarr[2] == tarr[3]) and
			(tarr[3] == tarr[4])
		) then
			local c = cpy(cur) 
			add(c,arr)
			add(lst,c)
			--dbg("base case/kan")
		elseif (
			(tarr[1] == tarr[2]) and
			(tarr[3] == tarr[4])
		) then
			local c = cpy(cur)
			add(c,{arr[1],arr[2]})
			add(c,{arr[3],arr[4]})
			add(lst,c)
			--dbg("base case/two pairs")
		end
		
		-- fixme bc4 needs to check
		-- for a single pair
		
		return
	end
	
	-- try forming a pair
	if (tarr[1] == tarr[2]) then
		local rest = {}
		for i = 3,#arr do
			rest[i-2] = arr[i]
		end
		local c = cpy(cur)
		add(c,{arr[1],arr[2]})
		add(lst,c)
		--dbg("adding pair")
		mkgrps(rest,c,lst);
	end
	
	-- try forming a triple
	if (
		tarr[1] == tarr[2] and
		tarr[2] == tarr[3]
	) then
		local rest = {}
		for i = 4,#arr do
			rest[i-3] = arr[i]
		end
		local c = cpy(cur)
		add(c,{arr[1],arr[2],arr[3]})
		add(lst,c)
		--dbg("adding triple")
		mkgrps(rest,c,lst);
	end
	
	-- try forming four-of-a-kind
	if (
		tarr[1] == tarr[2] and
		tarr[2] == tarr[3] and
		tarr[3] == tarr[4]
	) then
		local rest = {}
		for i = 5,#arr do
			rest[i-4] = arr[i]
		end
		local c = cpy(cur)
		add(c,{
			arr[1],
			arr[2],
			arr[3],
			arr[4]
		})
		add(lst,c)
		--dbg("adding kan")
		mkgrps(rest,c,lst);
	end
	
	-- try forming a run
	local run_found = true -- iupg
	local t1 = tarr[1]
	if (t1%16 > 7) then
		-- a run can't start above 7
		run_found = false
	end
	local i2 = afind(tarr,t1+1)
	local i3 = afind(tarr,t1+2)
	if (i2==nil or i3==nil) then
		run_found = false
	end
	
	if (run_found) then
		local rest = {}
		for i = 2,#arr do
			if (i~=i2 and i~=i3) then
				rest[#rest+1] = arr[i]
			end
		end
		--dbg("adding run")
		local c = cpy(cur)
		add(c,{arr[1],arr[i2],arr[i3]})
		add(lst,c)
		mkgrps(rest,c,lst)
	end
	
	-- try all groupings past this
	-- element
	local rest = {}
	for i = 2,#arr do
		rest[i-1] = arr[i]
	end
	
	mkgrps(rest,cur,lst)
	
	--dbg("base case/no options")
	return
end
